#include <array>
#include <iostream>

#include "Analysis/libEloss.h"
#include "TString.h"

using namespace std;

void convertToDataTrack()
{
 TString equalizer("Analysis/gainsTable_line.dat");
  
 bool _CF4_ = true;
 bool _iC4H10_ = true;
 bool _P10_ = true;

 if(_CF4_)
 { 
  const int nCF4 = 6;
  array<int,    nCF4> run_CF4    = {144,    182,    192,    257,    262,    284};
  array<double, nCF4> vdrift_CF4 = {0.1129, 0.0937, 0.0962, 0.1365, 0.0959, 0.09619};
 
  cout<<"\n\n******* Conversion of runs with CF4 ********\n\n";
 
  for(int kk=0; kk<nCF4; kk++)
  {
   cout<<"**** Conversion of RUN "<<run_CF4[kk]<<" ****\n";
   
   makeElossTreeWithProfiles(Form("Analysis/ardaConvert/ACTAR_%d.root", run_CF4[kk]),
  		         Form("Analysis/dataTrack/run%d_with_profiles.root",run_CF4[kk]),
  		         equalizer.Data(),vdrift_CF4[kk],130,25,0,0);
  }
 }
 
 if(_iC4H10_)
 { 	
  const int niC4H10 = 5;
  array<int,    niC4H10> run_iC4H10    = {147,     157,      246,     270,    281};
  array<double, niC4H10> vdrift_iC4H10 = {0.0451,  0.02592,	 0.01462, 0.0251, 0.02558};
 
  cout<<"\n\n******* Conversion of runs with iC4H10 ********\n\n";
 
  for(int kk=0; kk<niC4H10; kk++)
  {
   cout<<"**** Conversion of RUN "<<run_iC4H10[kk]<<" ****\n";   
   
   makeElossTreeWithProfiles(Form("Analysis/ardaConvert/ACTAR_%d.root", run_iC4H10[kk]),
  		         Form("Analysis/dataTrack/run%d_with_profiles.root",run_iC4H10[kk]),
  		         equalizer.Data(),vdrift_iC4H10[kk],130,25,0,0);
   	
  } 
 }

 if(_P10_)
 {   
  const int nP10 = 5;
  array<int,    nP10> run_P10    = {131,    172,    217,    253,	288};
  array<double, nP10> vdrift_P10 = {0.0452, 0.0421, 0.0444, 0.0448, 0.04468}; 
 
  cout<<"\n\n******* Conversion of runs with P10 ********\n\n";
 
  for(int kk=0; kk<nP10; kk++)
  {
   cout<<"**** Conversion of RUN "<<run_P10[kk]<<" ****\n";
   
   makeElossTreeWithProfiles(Form("Analysis/ardaConvert/ACTAR_%d.root", run_P10[kk]),
  		         Form("Analysis/dataTrack/run%d_with_profiles.root",run_P10[kk]),
  		         equalizer.Data(),vdrift_P10[kk],130,25,0,0);
   	
  } 
 }		        

}
