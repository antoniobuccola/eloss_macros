
#include "libEloss.h"

TH2F* BuildTrack(TTree *rawEventTree, cLookupTable *lookupTable, Int_t eventNumber, double th=0) 
{
  TH2F *pad = new TH2F(Form("pad_%d", eventNumber),Form("Pad Plane @ event: %d (without gains)", eventNumber), NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5);

  TTreeReader rdr(rawEventTree);
  TTreeReaderValue<cRawEvent> event(rdr, "event");

  if (rdr.SetEntry(eventNumber) != TTreeReader::EEntryStatus::kEntryValid) 
  {
    cout << "Error in retrieving the requested event" << endl;
    return NULL;
  }

  for (auto &hit: event->getHits()) 
  {
   int gcid = hit.getGlobalChannelId();
   cLookupTable::chanData hitInfo = lookupTable->getTable()[gcid];
   int chan = cLookupTable::getChanFromGlobalChannelId(gcid);
   
   if(hitInfo.getDet()!=0 || chan==11 || chan==22 || chan==45 || chan==56) 
   	 continue;
 
    double val = hit.getPeakHeight();
    if(val>th) pad->Fill(hitInfo.getCol(), hitInfo.getRow(), val);  
  }
  
  return pad;
}

double GetStart(TH1 *histo)
{
  int start=-1;
  do
  {
   ++start;
  }
  while(histo->GetBinContent(start)<=5);
  
  start = padSize*histo->GetBinCenter(start);
  #if DBGLV>0
  	cout<<"start (INSIDE): "<<start<<endl;
  #endif
  
  return (padSize+padPitch)*start;
}

/*********************************************************************/
/*********************************************************************/

TH2F* BuildTrackWithGains(TTree *rawEventTree, cLookupTable *lookupTable, TMatrixD gains, Int_t eventNumber, double th=0) 
{
  TH2F *pad = new TH2F(Form("pad_gains_%d", eventNumber), Form("Pad Plane @ event: %d (with gains)", eventNumber), NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5);
    	
  TTreeReader rdr(rawEventTree);
  TTreeReaderValue<cRawEvent> event(rdr, "event");

  if (rdr.SetEntry(eventNumber) != TTreeReader::EEntryStatus::kEntryValid) 
  {
    cout << "Error in retrieving the requested event" << endl;
    return NULL;
  }

  for (auto &hit: event->getHits()) 
  {
    cLookupTable::chanData hitInfo = lookupTable->getTable()[hit.getGlobalChannelId()];
	
    // Selects only hits from the pad
    if (hitInfo.getDet() != 0) continue;
  
    double val = hit.getPeakHeight();
    int my_col = hitInfo.getCol();
    int my_row = hitInfo.getRow();
    if(val>th) pad->Fill(my_col,my_row, val*gains[my_row][my_col]);  
  }
  
  return pad;
}

/*********************************************************************/
/*********************************************************************/

TH2F *BuildSideView(TTree *rawEventTree, cLookupTable *lookupTable, double vdrift, double delay, double Ts, Int_t eventNumber, double th=0)
{
  TH2F *sideView = new TH2F(Form("side_view_%d", eventNumber), Form("Side View @ event: %d", eventNumber), NCOLS, -0.5*padSize, padSize*(NCOLS-0.5), HEIGHT, -0.5, HEIGHT-0.5);
  
  TTreeReader rdr(rawEventTree);
  TTreeReaderValue<cRawEvent> event(rdr, "event");

  if (rdr.SetEntry(eventNumber) != TTreeReader::EEntryStatus::kEntryValid) 
  {
    cout << "Error in retrieving the requested event" << endl;
    return NULL;
  }

  for (auto &hit: event->getHits()) 
  {
    cLookupTable::chanData hitInfo = lookupTable->getTable()[hit.getGlobalChannelId()];
	
    // Selects only hits from the pad
    if (hitInfo.getDet() != 0) continue;
  
    double val = hit.getPeakHeight();
    if(val>th) 
    	sideView->Fill(hitInfo.getCol()*padSize, vdrift*Ts*(hit.getPeakTime()-delay), val);  
  }
  
  return sideView;
}

/*********************************************************************/
/*********************************************************************/

TMatrixD GetGains(TString Ltablefile, double G0=0)
{
 TMatrixD GAINS(NROWS,NCOLS);
 
 ifstream ltable;
 ltable.open(Ltablefile.Data(),ios::in);
 string str;
 getline(ltable, str);
 
 int row,col;
 double gain, offset;

 while(ltable>>col>>row>>gain>>offset)
 {
  GAINS[row][col]=gain; //gain
 }
 
 /* equalization */
 double Gref=GAINS[0][0];
 if(G0<0) Gref=1.;
 
 for(int y=0; y<NROWS; y++)
 {
  for(int x=0; x<NCOLS; x++)
  {
   if(GAINS[y][x]>0) //0
	   GAINS[y][x]=GAINS[y][x]/Gref;
   else 
   	   GAINS[y][x]=0.; //1
  }
 }
 
 ltable.close();
 
 return GAINS;
}

/*********************************************************************/
/*********************************************************************/

TH2F *MapOfGains(TString Ltablefile, double G0=-1)
{
 TH2F *hgains = new TH2F("hgains","hgains",NCOLS, -0.5,NCOLS-0.5, NROWS, -0.5, NROWS-0.5);
 
 TMatrixD GAINS=GetGains(Ltablefile, G0);
 
 for(int y=0; y<NROWS; y++)
 {
  for(int x=0; x<NCOLS; x++)
  {
   hgains->Fill(x,y, GAINS[y][x]);
  }
 }
 
 hgains->GetXaxis()->SetTitle("COLS");
 hgains->GetXaxis()->SetTitleOffset(0.7);
 hgains->GetXaxis()->SetTitleSize(0.06);

 hgains->GetYaxis()->SetTitle("ROWS");
 hgains->GetYaxis()->SetTitleOffset(0.7);
 hgains->GetYaxis()->SetTitleSize(0.06);
 
 return hgains;
}

/*********************************************************************/
/*********************************************************************/

pSignal *Histo2Sig(TH1 *histo)
{
 pSignal *sig = new pSignal(NCOLS);
 sig->SetTs(1.);
 
 for(int x=0; x<NCOLS; x++)
 {
  double cbin = histo->GetBinContent(x+1);
  sig->SetAt(cbin, x);
 }
 
 return sig;
}

/*********************************************************************/
/*********************************************************************/

double GetRangeGraph(TGraph *graph, double f)
{
 double range = 0.;
 
 const int N = graph->GetN();
 double *yy  = graph->GetY();

 int index     = LocMax(N, yy);
 double _ymax_ = yy[index];
  
 double threshold = f*_ymax_;
  
 do
 {
  index++;
 }while(yy[index]>threshold);  
 
 index--;

 double *xx = graph->GetX();
 range = xx[index];
 TSpline3 spgraph("spgraph", graph); 
  
 for(int rec = 0; rec<4; rec++)
 	range += (threshold - spgraph.Eval(range))/(spgraph.Derivative(range));
 
 return range;
}

/*********************************************************************/
/*********************************************************************/

double GetExtrapolatedRangeGraph(TGraph *graph)
{
 double rangeExt = 0.;
 
 TSpline3 spBragg("spBragg", graph);
 
 double range050 = GetRangeGraph(graph, 0.5);
 
 rangeExt = range050 - spBragg.Eval(range050)/spBragg.Derivative(range050);  
 
  #if DBGLV>0
 	graph->SetMarkerStyle(20);
 	graph->Draw("ap");
 	
 	TSpline3 *spDraw = new TSpline3(spBragg);	
 	spDraw->SetLineColor(kBlue);
 	spDraw->SetLineWidth(3);
 	spDraw->Draw("same");
 	
 	double spVal = spDraw->Eval(range050);
 	TMarker *_my_marker_ = new TMarker(range050, spVal, 20);
 	_my_marker_->SetMarkerSize(1.5);
 	_my_marker_->SetMarkerColor(kRed+3);
 	_my_marker_->Draw("p");
 	
 	TF1 *_my_line_ = new TF1("_my_line_","pol1",0,120);
 	double _m_ = spDraw->Derivative(range050);
 	_my_line_->SetParameter(1, _m_);
 	_my_line_->SetParameter(0, -range050*_m_+spVal);
 	_my_line_->Draw("same");
 #endif
 
 return rangeExt;
}

/*********************************************************************/
/*********************************************************************/

array<double, 3> GetExtrapolatedRangeGraphErrors(TGraphErrors *graph)
{
 array<double, 3> rangeExt = {0.,0., 0.};
 
 const int N = graph->GetN();
 double *x = graph->GetX();
 double *y = graph->GetY();
 double *ey = graph->GetEY();
 
 TGraph *center = new TGraph(N);
 TGraph *low = new TGraph(N);
 TGraph *high = new TGraph(N); 
 
 for(int ii=0; ii<N; ii++)
 {
  center->SetPoint(ii, x[ii] , y[ii]);
  low->SetPoint(ii, x[ii] , y[ii]-ey[ii]);
  high->SetPoint(ii, x[ii] , y[ii]+ey[ii]);
 }
 
 double rangeC = GetExtrapolatedRangeGraph(center);
 double rangeL = GetExtrapolatedRangeGraph(low);
 double rangeH = GetExtrapolatedRangeGraph(high);
 
 #if DBGLV>0
  	graph->SetMarkerStyle(20);
  	graph->SetMarkerColor(kBlack);
 	graph->Draw("ap");
 	
 	TSpline3 *spcenter = new TSpline3("spcenter", center);
 	spcenter->SetLineColor(kRed);	
 	spcenter->Draw("same");
 	
 	TSpline3 *splow = new TSpline3("splow", low);
 	splow->SetLineColor(kBlue);
 	splow->Draw("same");
 	
 	TSpline3 *sphigh = new TSpline3("sphigh", high);	
 	sphigh->SetLineColor(kGreen);
 	sphigh->Draw("same");	
 	
 	TF1 *_my_lineC_ = new TF1("_my_lineC_","pol1",0,120);
 	double rC = GetRangeGraph(center, 0.5);
 	double sC = spcenter->Eval(rC);
 	double _mC_ = spcenter->Derivative(rC);
 	_my_lineC_->SetParameter(1, _mC_);
 	_my_lineC_->SetParameter(0, -rC*_mC_+sC);
 	_my_lineC_->SetLineColor(kRed);
 	_my_lineC_->Draw("same");
 	
 	TMarker *_my_marker_C = new TMarker(rC, sC, 20);
 	_my_marker_C->SetMarkerSize(1.5);
 	_my_marker_C->SetMarkerColor(kRed);
 	_my_marker_C->Draw("p");

 	TF1 *_my_lineL_ = new TF1("_my_lineL_","pol1",0,120);
 	double rL = GetRangeGraph(low, 0.5);
 	double sL = splow->Eval(rL);
 	double _mL_ = splow->Derivative(rL);
 	_my_lineL_->SetParameter(1, _mL_);
 	_my_lineL_->SetParameter(0, -rL*_mL_+sL);
 	_my_lineL_->SetLineColor(kBlue);
 	_my_lineL_->Draw("same");
 	
	TMarker *_my_marker_L = new TMarker(rL, sL, 20);
 	_my_marker_L->SetMarkerSize(1.5);
 	_my_marker_L->SetMarkerColor(kBlue);
 	_my_marker_L->Draw("p");

 	TF1 *_my_lineH_ = new TF1("_my_lineH_","pol1",0,120);
 	double rH = GetRangeGraph(high, 0.5);
 	double sH = sphigh->Eval(rH);
 	double _mH_ = sphigh->Derivative(rH);
 	_my_lineH_->SetParameter(1, _mH_);
 	_my_lineH_->SetParameter(0, -rH*_mH_+sH);
 	_my_lineH_->SetLineColor(kGreen);
 	_my_lineH_->Draw("same");
 	
	TMarker *_my_marker_H = new TMarker(rH, sH, 20);
 	_my_marker_H->SetMarkerSize(1.5);
 	_my_marker_H->SetMarkerColor(kGreen);
 	_my_marker_H->Draw("p");
 #endif
   
 rangeExt[0]=rangeC;
 rangeExt[1]=rangeC-rangeL;
 rangeExt[2]=rangeH-rangeC;
  
 delete center;
 delete low;
 delete high;
 
 return rangeExt;
}

/*********************************************************************/
/*********************************************************************/

double GetMaxGraph(TGraph *gr)
{
 const int N = gr->GetN();
 double *yy = gr->GetY();
 int locmax = LocMax(N, yy);
 double _ymax_ = yy[locmax];
 return _ymax_;
}

/*********************************************************************/
/*********************************************************************/

TGraph *ATS2Physics(TH1 *histo, double scaling=0.5)
{
  #if DBGLV>0
  	cout<<"---------- INSIDE ATS2Physics ----------\n";
  #endif
 pSignal *s = Histo2Sig(histo);
 
 #if DBGLV>0
  	cout<<"---------- SCALING  ----------\n";
  #endif
 s->Scale(scaling);
 s->SetTs(padSize+padPitch);
  #if DBGLV>0
  	cout<<"---------- To Graph  ----------\n";
  #endif
 TGraph *profile = s->Sig2Graph();
  #if DBGLV>0
  	cout<<"---------- Some fancy settings  ----------\n";
  #endif
 profile->SetName(histo->GetName());
 profile->GetXaxis()->SetTitle("depth (mm)");
 profile->SetMarkerStyle(21);
 profile->SetMarkerSize(1.);
 delete s;
 return profile;
}

/*********************************************************************/
/*********************************************************************/

double TransverseWidth(TH2 *track)
{
 double width=0.;
 
 const double xmin = track->GetXaxis()->GetXmin();
 const double xmax = track->GetXaxis()->GetXmax();
 
 TProfile *projTrack = track->ProfileX("projTrack");
 
 TF1 line("line","pol1", xmin, xmax);
 line.SetParameter(0, 16);
 line.SetParameter(1, 0);
 
 projTrack->Fit("line", "RNQC");
 
 double q = line.GetParameter(0);
 double m = line.GetParameter(1);
 
 TH1D projDist("projDist","projDist", 32,-16, 16);
 
 for(int yy=1; yy<=NROWS; yy++)
 {
  for(int xx=1; xx<=NCOLS; xx++)  
  {
   int idbin = track->GetBin(xx,yy);
   
   double contBin = track->GetBinContent(idbin);
   if(contBin>0)
   {
    double x = xx - 1;
    double y = yy - 1;
   
    double dist = (y-m*x-q)/Sqrt(1+m*m);
    projDist.Fill(dist, contBin);
   }
  }
 }
 
 TF1 g("g","gaus", -16, 16);
 g.SetParameter(0, projDist.GetMaximum());
 g.SetParameter(1, 0);
 g.SetParameter(2, 2);
 projDist.Fit("g","RNQ");
 
 width = g.GetParameter(2)*2.35*2;
 #if DBGLV>2
 	TH1D *hcopy = new TH1D(projDist);
 	hcopy->Draw("histo");
 	return width;
 #endif

 delete projTrack;
 
 return width;
}


/*********************************************************/
/*********************************************************/
/*********************************************************/
/*********************************************************/

array<TGraphErrors*, 2> elossVsSpeed(TGraphErrors *ats, TGraph *srim, int A)
{
 TGraphErrors *g1 = new TGraphErrors();
 TGraphErrors *g2 = new TGraphErrors();
 
 array<TGraphErrors*, 2> graphs = {g1, g2};
 
 /*
 	graphs[0] <---->  ATS
 	graphs[1] <---->  SRIM
 */
 
 TSpline3 spSrim("spSrim", srim);
 
 double *x = ats->GetX();
 
 double *dEdx = ats->GetY(); 
 double *edEdx = ats->GetEY();
 
 double energy = IntegrateProfile(srim, 0,130);
 
 double Eion = energy;
 int kk=0;
 
 while(dEdx[kk]>0)
 {
  Eion = energy - IntegrateProfile(srim, 0,x[kk]);
  
  //cout<<"x: "<<x[kk]<<" E: "<<Eion<<endl;
  
  if(Eion>=0)
  {
   double v = Clight*TMath::Sqrt(2*Eion/(A*AMU));
   
   graphs[0]->SetPoint(kk, v, dEdx[kk]);
   graphs[0]->SetPointError(kk, 0, edEdx[kk]);
   
   graphs[1]->SetPoint(kk, v, spSrim.Eval(x[kk]));
   graphs[1]->SetPointError(kk, 0, 0);
  }  
  kk++;
 }
 
 
 
 for(int jj=0; jj<2; jj++)
 { 
  graphs[jj]->GetXaxis()->SetTitle("Speed (mm/ns)");
  graphs[jj]->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
 }
 
 graphs[0]->SetMarkerStyle(20);
 graphs[0]->SetMarkerColor(kRed+1);
 graphs[0]->SetLineWidth(2);
 graphs[0]->SetLineColor(kRed+1); 
 
 graphs[1]->SetLineWidth(4);
 graphs[1]->SetLineColor(kGreen+2); 
  
 return graphs;
}


TGraphErrors *elossVsEnergyATS(TGraphErrors *profile)
{
 TGraphErrors *g = new TGraphErrors();
 
 TSpline3 spProfile("spProfile", profile);
 double *edEdx = profile->GetEY();
 
 double energy = IntegrateProfile(profile, 0,130);
 int kk=0;
 
 double Eion = energy;
 
 while(Eion>=0)
 {
  Eion = energy - IntegrateProfile(profile, 0, 2*kk);
  g->SetPoint(kk, Eion, spProfile.Eval(2*kk));
  g->SetPointError(kk,0,edEdx[kk]);
  kk++;
 }
 
 g->SetMarkerStyle(20);
 g->SetMarkerColor(kRed+2);
 g->SetLineColor(kRed+2);
 g->SetLineWidth(3);
 
 g->GetXaxis()->SetTitle("Residual Energy (keV)");
 g->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
   
 
 return g;
}

/**********************/

TGraphErrors *stoppingXsection_vs_energy(TGraphErrors *profile, double density, double gas_molar_mass)
{
 const double amu = 1.66e-24;
 double convert = 1e13*1e4*gas_molar_mass*amu/density;
 
 TGraphErrors *g1 = elossVsEnergyATS(profile);
 
 TGraphErrors *se = ScaleYErrors(g1, convert, 0);
 
 se->SetMarkerColor(kRed);
 se->SetMarkerSize(1.5);
 
 se->SetLineColor(kRed);
 se->SetLineWidth(3);
 
 se->GetXaxis()->SetTitle("Energy (keV)");
 se->GetYaxis()->SetTitle("Stopping Cross section (10^{-13} eV cm^{2}) ");
 
 return se;
}

/**********************/

TGraph *elossVsEnergySRIM(TGraph *profile)
{
 TGraph *g = new TGraph();

 TSpline3 spProfile("spProfile", profile);
 
 double energy = IntegrateProfile(profile, 0,130);
 int kk=0;
 
 double Eion = energy;
 
 while(Eion>=0)
 {
  Eion = energy - IntegrateProfile(profile, 0, 2*kk);
  g->SetPoint(kk, Eion, spProfile.Eval(2*kk));
  kk++;
 }
 
 g->SetMarkerStyle(20);
 g->SetMarkerColor(kGreen+2);
 g->SetLineColor(kGreen+2);
 g->SetLineWidth(3);
 
 g->GetXaxis()->SetTitle("Residual Energy (keV)");
 g->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
 
 return g;
}


/*

TGraphErrors *elossVsSpeedATS(TGraphErrors *profile, int A)
{
 TGraphErrors *g = new TGraphErrors(); 

 TSpline3 spProfile("spProfile", profile);
 double *edEdx = profile->GetEY();
 
 double energy = IntegrateProfile(profile, 0,130);
 int kk=0;
 
 double Eion = energy;
 
 while(Eion>=0)
 {
  Eion = energy - IntegrateProfile(profile, 0, 2*kk);
  if(Eion>=0)
  {
   double speed = Clight*TMath::Sqrt(2*Eion/(A*AMU));
   g->SetPoint(kk, speed, spProfile.Eval(2*kk));
   g->SetPointError(kk,0,edEdx[kk]);
  }

  kk++;
 }
 
 g->SetMarkerStyle(20);
 g->SetMarkerColor(kRed+2);
 g->SetLineColor(kRed+2);
 g->SetLineWidth(3);
 
 g->GetXaxis()->SetTitle("Residual Energy (keV)");
 g->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
 
 return g;
}

TGraph *elossVsSpeedSRIM(TGraph *profile, int A)
{
 TGraph *g = new TGraph();
  
 TSpline3 spProfile("spProfile", profile);
 
 double energy = IntegrateProfile(profile, 0,130);
 int kk=0;
 
 double Eion = energy;
 
 while(Eion>=0)
 {
  Eion = energy - IntegrateProfile(profile, 0, 2*kk);
  
  if(Eion>=0)
  {
   double speed = Clight*TMath::Sqrt(2*Eion/(A*AMU));
   g->SetPoint(kk, speed, spProfile.Eval(2*kk));
  }
  
  kk++;
 }
 
 g->SetMarkerStyle(20);
 g->SetMarkerColor(kGreen+2);
 g->SetLineColor(kGreen+2);
 g->SetLineWidth(3);
 
 g->GetXaxis()->SetTitle("Speed (cm/ns)");
 g->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
 
 return g; 
}

*/

/*********************************************************************/
/*********************************************************************/

TGraph *DiffusionWidth(TH2* track)
{
 TGraph *diffusion = new TGraph(NCOLS);
 
 TF1 g("g","gaus",integral_do, integral_up);
 g.SetParameter(0,2000);
 g.SetParameter(1, 16);
 g.SetParameter(2, 2);
 
 for(int xx=0; xx<NCOLS; xx++)
 {
  TH1D *hp = track->ProjectionY(Form("hp%d", xx), xx, xx+1);
  if(hp->GetEntries()<3)
  {
   delete hp;
   diffusion->SetPoint(xx, 1+(padSize+padPitch)*xx, 0);
   continue;
  }
  
  hp->Fit("g", "RNQ");
  
  double sigma = g.GetParameter(2);
  #if DBGLV>0
  	cout<<"sigma @ x="<<kk*2<<" : "<<sigma<<endl;
  	if(sigma<0.05)
  		cout<<"-------> ERROR @ x="<<kk*2<<" : "<<sigma<<endl;
  #endif
  /*
  double sum=0;
  
  for(int yy=0; yy<NROWS; yy++)
  {  
   int idbin = track->GetBin(xx, yy);
   double cbin = track->GetBinContent(idbin);
   
   if(cbin>0)
   	sum+=cbin;
  }
  double sigma = 1./Sqrt(sum);
  */
  
  diffusion->SetPoint(xx, 1+(padSize+padPitch)*xx, 2.*sigma);

 }
 
 diffusion->SetMarkerStyle(20);
 return diffusion;
}

/*********************************************************************/
/*********************************************************************/

double GetTheta(TH2 *track, int a=0, int b=NROWS)
{
 double theta=0.;
 
 TProfile *pad_hpf = track->ProfileX("pad_hpf", a, b);
 
 TF1 lin("lin","pol1",-0.5, NCOLS-0.5);
 lin.SetParameter(0, 16);
 lin.SetParameter(1,  0); 
 pad_hpf->Fit("lin","RNQC");
 
 double _m_ = lin.GetParameter(1);
 theta = ATan(_m_)*RadToDeg();   
 
 #if DBGLV>0
 	cout<<"theta (INSIDE): "<<theta<<endl;
 #endif
  
 delete pad_hpf; 
 return theta;
}

/*********************************************************************/
/*********************************************************************/

double GetPhi(TH2 *side, int a, int b)
{
 double phi=0.;
 
 TProfile *pad_hpf = side->ProfileX("pad_hpf", a, b);
 
 TF1 lin("lin","pol1",-1., 2*(NCOLS-0.5));
 lin.SetParameter(0, 20);
 lin.SetParameter(1,  0); 
 pad_hpf->Fit("lin","RNQC");
 
 double _m_ = lin.GetParameter(1);
 phi = ATan(_m_)*RadToDeg();   
 
 #if DBGLV>0
 	cout<<"phi (INSIDE): "<<phi<<endl;
 #endif
  
 delete pad_hpf; 
 return phi;
}

/*********************************************************************/
/*********************************************************************/
TGraph *srimBragg(const char *filename)
{  
 ifstream srimFile;
 srimFile.open(filename, ios::in);
 string str;
 getline(srimFile, str);
 getline(srimFile, str);
 getline(srimFile, str);
 double x, dEdx_ion, dEdx_recoil;
 
 TGraph *braggSRIM = new TGraph();
 int i=0;
 while(srimFile>>x>>dEdx_ion>>dEdx_recoil)
 {
  x*=1e-7;
  if(x>=_START_)
  {
   dEdx_ion*=1e4;
   braggSRIM->SetPoint(i, x-_START_, dEdx_ion);
   i++;
  }
 }
 srimFile.close();
 braggSRIM->SetTitle("srimBragg");
 braggSRIM->SetName("srimBragg");
 braggSRIM->GetXaxis()->SetTitle("depth (mm)");
 braggSRIM->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
 braggSRIM->GetYaxis()->SetTitleOffset(1.1);
 braggSRIM->SetMarkerStyle(20);
 braggSRIM->SetMarkerColor(kGreen+1); 
 braggSRIM->SetLineColor(kGreen+1); 
 braggSRIM->SetLineWidth(3);
 
 return braggSRIM;
}

/*********************************************************************/
/*********************************************************************/

TGraph *Diffuse(const char *filename, double sigma)
{
 ifstream srimFile;
 srimFile.open(filename, ios::in);
 string str;
 getline(srimFile, str);
 getline(srimFile, str);
 getline(srimFile, str);

 int i=0; 
 double xx, dEdx_ion, dEdx_recoil;
 
 TGraph *braggSRIM = new TGraph();
 while(srimFile>>xx>>dEdx_ion>>dEdx_recoil)
 {
  xx*=1e-7;
  dEdx_ion*=1e4;
  dEdx_recoil*=1e4;
  braggSRIM->SetPoint(i, xx, dEdx_ion);
  i++;
 }
 srimFile.close();
 
 TSpline3 spBragg("spBragg",braggSRIM); // S(x)
 
 TGraph *braggSampled = new TGraph();
 
 const double dx = 0.2;
 double x=0;
 int jj=0;
 
 do
 {
  if(braggSRIM->Eval(x)>0)
  	braggSampled->SetPoint(jj, x, IntegrateProfile(spBragg, x-dx/2, x+dx/2)/dx);	  
  else
  	braggSampled->SetPoint(jj, x, 0.);

  jj++;
  x+=dx;
 }
 while(x<200.);
 
 int n = braggSampled->GetN();
 double *xs = braggSampled->GetX();
 double *ys = braggSampled->GetY();
 
 TGraph *Diffuse = new TGraph();
 
 for(int k=0; k<n; k++)
 {
  double de_post_diff = 0.;
  
  for(int j=0; j<n; j++)
  	de_post_diff += ys[j]*(ROOT::Math::normal_cdf(xs[k]+dx/2, sigma, xs[j])-ROOT::Math::normal_cdf(xs[k]-dx/2, sigma, xs[j]));
  
   Diffuse->SetPoint(k, xs[k], de_post_diff);
 }
  
 double *xp = Diffuse->GetX();
 double *yp = Diffuse->GetY();
 
 TGraph *srimPP = new TGraph();
 TGraph *diff0 = new TGraph();
 
 int hh=0;
 int gg=0;
 
 for(int jj=0; jj<n; jj++)
 {
  if(xs[jj]>=50.)
  {
   srimPP->SetPoint(hh, xs[jj]-50., ys[jj]);
   hh++;
  }
  if(xp[jj]>=50.)
  {
   if(yp[jj]>=0)
   	diff0->SetPoint(gg, xp[jj]-50., yp[jj]);
   else
   	diff0->SetPoint(gg, xp[jj]-50., 0.);
   gg++;
  }
 }
 
 double E1 = IntegrateProfile(braggSRIM, 50,GetRangeGraph(braggSRIM, 0.00001));
 //double E2 = IntegrateProfile(braggSampled, 50,180);
 //double Esrim = IntegrateProfile(srimPP, 0,130);
 double Ediff = IntegrateProfile(diff0, 0,GetRangeGraph(braggSRIM, 0.00001));
 
  double sc = E1/Ediff;
 TGraph *diffPP = ScaleY(diff0, sc);
 
 /*
 srimPP->SetLineWidth(4);
 srimPP->SetLineColor(kGreen);
 */
 
 diffPP->SetLineWidth(3);
 diffPP->SetLineColor(kBlack);
 diffPP->SetMarkerStyle(20);
 diffPP->SetMarkerSize(1.2);
 diffPP->SetMarkerColor(kBlack);
 
 /*
 srimPP->Draw("al");
 diffPP->Draw("p");
 srimPP->Draw("l");
 */
  
 return diffPP;
}

/*********************************************************************/
/*********************************************************************/


double GetYaverage(TH2 *track)
{
  int cc=0;
  double sum=0;
  
  for(int x=1; x<=NCOLS; x++)
  {
   int counter=0;
   for(int y=1; y<=NROWS; y++)
   {
    int idbin = track->GetBin(x,y);
    double cbin = track->GetBinContent(idbin);
    
    if(cbin>0) counter++;
   }
   if(counter>0)
   {
    cc++;
    sum+=counter;
   }	
  }
  
  return sum/cc;  
}

/*********************************************************************/
/*********************************************************************/

double IntegrateProfile(TSpline3 profile, double a, double b)
{
 double integral=0;
 const double s = (b-a)/nInt;
 double x = a;
 while(x<b)
 {
  integral += (s/6)*(profile.Eval(x) + profile.Eval(x+s) + 4*profile.Eval((2*x+s)/2));
  x+=s;
 }
 return integral;
}

/*********************************************************************/
/*********************************************************************/

double IntegrateProfile(TGraph *profile, double a, double b)
{
 TSpline3 sp("sp", profile);
 return IntegrateProfile(sp, a, b);
}

/*********************************************************************/
/*********************************************************************/

array<double, 2> IntegrateProfileWithErrors(TGraphErrors *profile, double a, double b)
{
 int N = profile->GetN();
 double *x = profile->GetX();
 double *y = profile->GetY();
 double *ey = profile->GetEY();
 
 TGraph *center = new TGraph();
 TGraph *high = new TGraph();
 TGraph *low = new TGraph();
 
 for(int jj=0; jj<N; jj++)
 {
  center->SetPoint(jj, x[jj], y[jj]);
  high->SetPoint(jj, x[jj], y[jj]+ey[jj]);
  low->SetPoint(jj, x[jj], y[jj]-ey[jj]);
 } 
 array<double, 2> integral = {0}; 
 
 double I = IntegrateProfile(center, a,b);
 double Ip = IntegrateProfile(high, a,b);
 double Im = IntegrateProfile(low, a,b);
 
 /*
 cout<<I<<endl;
 cout<<Ip<<endl;
 cout<<Im<<endl;
 
 cout<<Ip-I<<"   "<<I-Im<<endl;
 
 center->SetLineColor(kGreen);
 center->SetLineWidth(3);

 high->SetLineColor(kRed);
 high->SetLineWidth(3); 
 
 low->SetLineColor(kBlue);
 low->SetLineWidth(3); 
 
 center->Draw("al");
 high->Draw("l");
 low->Draw("l");
 */
 
 integral[0] = I;
 integral[1] = max(Ip-I, I-Im);
 
 return integral;
}

/*********************************************************************/
/*********************************************************************/


TGraph *RangeVsCharge(TSpline3 braggSpline, double start, double step = padSize/100.)
{
 vector<double> charge;
 vector<double> distance;
 
 for(double x = start; x>=0; x-=step)
 {
  double d = start - x;
  double q = IntegrateProfile(braggSpline, x, start);
  charge.push_back(q);
  distance.push_back(d);
 }
 
 TGraph *graph = new TGraph(charge.size(), &distance[0], &charge[0]);
 graph->SetMarkerStyle(20);
 graph->SetMarkerSize(0.7);
 
 return graph;
}

/*********************************************************************/
/*********************************************************************/

TGraph *RangeVsCharge(TGraph *braggGraph,double scale, double step = padSize/100.)
{
 double _max_ = GetMaxGraph(braggGraph);
 TGraph *scBraggGraph = ScaleY(braggGraph, scale);
 TSpline3 braggSpline("braggSpline", scBraggGraph);
 double start = 20;
 do
 {
  start += step;
 }
 while(braggSpline.Eval(start)>_FRACTION_*_max_);
 
 return RangeVsCharge(braggSpline, start, step);
}

/*********************************************************************/
/*********************************************************************/


TGraph *RangeVsCharge(TH1 *braggProfile, double scale, double step = padSize/100.)
{
 TGraph *braggGraph = ATS2Physics(braggProfile, scale); 
 double _max_ = GetMaxGraph(braggGraph);
 TSpline3 braggSpline("braggSpline", braggGraph);
 double start = 20;
 do
 {
  start += step;
 }
 while(braggSpline.Eval(start)>_FRACTION_*_max_);
 delete braggGraph;
 return RangeVsCharge(braggSpline, start, step);
}

/*********************************************************************/
/*********************************************************************/

double ComputeCalibration(TGraph *ats, TGraph *srim) 
{
 double A=0;
 
 int Nats = ats->GetN();
 double *xats = ats->GetX();
 double *yats = ats->GetY(); 
 int locats = LocMax(Nats, xats);
 double _maxats_ = xats[locats];
 
 int Nsrim = srim->GetN();
 
 double *xsrim = srim->GetX();
 double *ysrim = srim->GetY(); 
 int locsrim = LocMax(Nsrim, xsrim);
 double _maxsrim_ = xsrim[locsrim];
 
 double _XTH_ = min(_maxats_, _maxsrim_);
 
 TSpline3 spSRIM("spSRIM", srim);
 #if DBGLV>0
  	cout<<"inside special calibration computation \n";
  #endif
 double num=0;
 double den=0;
 int kk=0;
 do
 {
  if(yats[kk]>0)
  {
   num += yats[kk]*spSRIM.Eval(xats[kk]);
   den += yats[kk]*yats[kk]; 
  }
     kk++;
 }while(xats[kk]<_XTH_);
 
 A=num/den;
  #if DBGLV>0
  	cout<<"A (inside) = "<<A<<endl;
  #endif
 return A;
}

/*********************************************************************/
/*********************************************************************/

TGraphErrors *RangeVsChargeWithErrors(TGraphErrors *braggGraph, double step) /* distances in mm */
{ 
 double *xats = braggGraph->GetX();
 
 double *yats = braggGraph->GetY();
 double *eyats = braggGraph->GetEY();
 
 TGraph *braggGraphLow = new TGraph();
 TGraph *braggGraphHigh = new TGraph(); 
 
 for(int hh=0; hh<NCOLS; hh++)
 {
  braggGraphLow->SetPoint(hh,xats[hh],yats[hh]-eyats[hh]);
  braggGraphHigh->SetPoint(hh,xats[hh],yats[hh]+eyats[hh]);   
 }
  
 #if DBGLV>1
 	TCanvas *first = new TCanvas("BraggProfiles","Bragg Profiles");
 	first->cd();
 	braggGraph->SetMarkerStyle(20);
 	braggGraph->SetMarkerColor(kBlack);
 	braggGraph->Draw("ap");
 	braggGraphLow->SetMarkerStyle(20);
 	braggGraphLow->SetMarkerColor(kBlue+3);
	braggGraphLow->Draw("p");
	braggGraphHigh->SetMarkerStyle(20);
	braggGraphHigh->SetMarkerColor(kRed+1);
	braggGraphHigh->Draw("p");
 #endif
 
 double _max_ = GetMaxGraph(braggGraph);
 TSpline3 braggSpline("braggSpline", braggGraph);
 
 double start = 20;
 do
 {
  start += step;
 }
 while(braggSpline.Eval(start)>_FRACTION_*_max_);
 
 TSpline3 spCenter("spCenter", braggGraph);
 TSpline3 spLow("spLow", braggGraphLow);
 TSpline3 spHigh("spHigh", braggGraphHigh);
   
 TGraph *integralCenter = RangeVsCharge(spCenter, start, step);
 TGraph *integralLow =  RangeVsCharge(spLow, start, step);
 TGraph *integralHigh =  RangeVsCharge(spHigh, start, step);
 
 #if DBGLV>1
 	TCanvas *second = new TCanvas("Correlations","Correlations");
 	second->cd();
 	integralCenter->SetMarkerStyle(20);
 	integralCenter->SetMarkerColor(kBlack);
 	integralCenter->Draw("ap");
 	integralLow->SetMarkerStyle(20);
 	integralLow->SetMarkerColor(kBlue+3);
	integralLow->Draw("p");
	integralHigh->SetMarkerStyle(20);
	integralHigh->SetMarkerColor(kRed+1);
	integralHigh->Draw("p");
 #endif
 
  #if DBGLV>1
 	TCanvas *third = new TCanvas("braggSplines","braggSplines");
 	third->cd();
 	
 	TSpline3 *spCenterD = new TSpline3(spCenter);
 	spCenterD->SetLineWidth(3);
 	spCenterD->SetLineColor(kBlack);
 	spCenterD->Draw("al");
 	
 	TSpline3 *spLowD = new TSpline3(spLow);
 	spLowD->SetLineWidth(3);
 	spLowD->SetLineColor(kBlue+3);
 	spLowD->Draw("same");
 	
 	TSpline3 *spHighD = new TSpline3(spHigh);
 	spHighD->SetLineWidth(3);
 	spHighD->SetLineColor(kRed+1);
 	spHighD->Draw("same");
 #endif
  
 int nIntegral = integralCenter->GetN(); 
 double *xIntegral = integralCenter->GetX();
 double *yIntCenter = integralCenter->GetY();
 double *yIntHigh = integralHigh->GetY();
 double *yIntLow = integralLow->GetY(); 
 
 TGraphErrors *FINALE = new TGraphErrors();
 for(int hh=0; hh<nIntegral; hh++)
 {
  FINALE->SetPoint(hh, xIntegral[hh], yIntCenter[hh]);

  double eVal1 = Abs((yIntHigh[hh]-yIntCenter[hh]));
  double eVal2 = Abs((yIntLow[hh]-yIntCenter[hh]));
  double eVal = max(eVal2, eVal1);
  
  FINALE->SetPointError(hh, 0, eVal);
 }

 #if DBGLV>0
	  return FINALE;
 #endif 
 
 delete braggGraphLow;
 delete braggGraphHigh;
 delete integralHigh;
 delete integralLow;
 delete integralCenter;
  
 return FINALE;
}

/*********************************************************************/
/*********************************************************************/

array<double,2> ComputeCalibrationAndError(TGraphErrors *ats, TGraph *srim) 
{ 
 
 array<double, 2> calib = {0};
 
 int Nats = ats->GetN();
 double *xats = ats->GetX();
 double *yats = ats->GetY(); 
 double *eyats = ats->GetEY();
 int locats = LocMax(Nats, xats);
 double _maxats_ = xats[locats];
 
 int Nsrim = srim->GetN();
 double *xsrim = srim->GetX();
 double *ysrim = srim->GetY(); 
 int locsrim = LocMax(Nsrim, xsrim);
 double _maxsrim_ = xsrim[locsrim];
 
 double _XTH_ = min(_maxats_, _maxsrim_);
 
 TSpline3 spSRIM("spSRIM", srim);
  
 double num=0;
 double errnum=0;
 double den=0;
 int kk=0;
 double errval=0;
 do
 {
  errval = eyats[kk];
  if(errval!=0 && yats[kk]!=0)
  {
   num += (yats[kk]*spSRIM.Eval(xats[kk]))/(errval*errval);
   errnum +=  spSRIM.Eval(xats[kk])/errval;
   den += (spSRIM.Eval(xats[kk])*spSRIM.Eval(xats[kk]))/(errval*errval); 
  }
  else
  {
   #if DBGLV>0
   	cout<<"\n\nx = "<<xats[kk]<<"   ERRORE ERRORE ERRORE\n\n";
   #endif
  }
  kk++;
 }while(xats[kk]<_XTH_);
 
 double B = num/den;
 double DB = errnum/den;
 
 calib[0] = 1./B;
 calib[1] = DB/(B*B);
 
 #if DBGLV>0
	 cout<<"num: "<<num<<endl;
	 cout<<"den: "<<den<<endl;
	 cout<<"B: "<<B<<endl;
	 cout<<"cal: "<<calib[0]<<endl;
 #endif
 return calib;
}
//
/*********************************************************************/
/*********************************************************************/

TGraphErrors *ScaleYErrors(TGraphErrors *gr, double scale, double escale=0.)
{
 int N = gr->GetN();

 double *xats = gr->GetX();
 double *exats = gr->GetEX(); 
 
 double *yats = gr->GetY(); 
 double *eyats = gr->GetEY();

 TGraphErrors *grnew = new TGraphErrors(N);
 for(int kk=0; kk<N; kk++)
 {
  double val = yats[kk]*scale;
  grnew->SetPoint(kk, xats[kk], val);
  
  double eval = eyats[kk]*scale + yats[kk]*escale;
  grnew->SetPointError(kk, exats[kk], eval); 
 }
 
 grnew->SetMarkerStyle(20);
 grnew->SetMarkerSize(1.4);
 grnew->SetMarkerColor(kRed);
 grnew->SetLineColor(kRed);
 grnew->SetLineWidth(3);
  
 return grnew;
}

/*********************************************************************/
/*********************************************************************/

TGraphErrors *ShiftXwithErrors(TGraphErrors *gr, double shift)
{
 TGraphErrors *shifted = new TGraphErrors();
 
 int n = gr->GetN();
 double *x = gr->GetX();
 double *y = gr->GetY();
 double *ex = gr->GetEX();
 double *ey = gr->GetEY();
 
 for(int kk=0; kk<n; kk++)
 {
  shifted->SetPoint(kk, x[kk]+shift, y[kk]);
  shifted->SetPointError(kk, ex[kk], ey[kk]);
 }
 
 shifted->SetMarkerStyle(20);
 shifted->SetMarkerSize(1.5);
 shifted->SetMarkerColor(kRed+2);
 shifted->SetLineColor(kRed+2);
 shifted->SetLineWidth(3);
 
 return shifted;
}

/*********************************************************************/
/*********************************************************************/

TGraph *ShiftX(TGraph *gr, double shift)
{
 TGraph *shifted = new TGraph();
 
 int n = gr->GetN();
 double *x = gr->GetX();
 double *y = gr->GetY();
 
 for(int kk=0; kk<n; kk++)
 {
  shifted->SetPoint(kk, x[kk]+shift, y[kk]);
 }
 
 shifted->SetMarkerStyle(20);
 shifted->SetMarkerSize(1.5);
 shifted->SetMarkerColor(kRed+2);
 shifted->SetLineColor(kRed+2);
 shifted->SetLineWidth(3);
 
 return shifted;
}

/*********************************************************************/
/*********************************************************************/

TGraph *ScaleY(TGraph *gr, double scale)
{
 int N = gr->GetN();
 double *yats = gr->GetY(); 
 double *xats = gr->GetX();
 
 vector<double> ynew;
 
 for(int kk = 0; kk<N; kk++)
 	ynew.push_back(scale*yats[kk]);

 TGraph *grnew = new TGraph(N, xats, &ynew[0]);
 
 grnew->SetMarkerStyle(20);
 grnew->SetMarkerColor(kRed+2);
 grnew->SetLineColor(kRed+2);
 grnew->SetLineWidth(3);

 return grnew;
}

/*********************************************************************/
/*********************************************************************/

TGraphErrors *ComputeAbsoluteResiduals(TGraphErrors *g1, TGraph *g2)
{ 
 /*
 	g1 : exp
 	g2 : model
 */ 
 int N1= g1->GetN();
 double *x1 = g1->GetX();
 double *y1 = g1->GetY(); 
 int loc1 = LocMax(N1, x1);
 double _max1_ = x1[loc1];
 double _th1_ = 0.01*GetMaxGraph(g1);
 
 int N2 = g2->GetN();
 double *x2 = g2->GetX();
 double *y2 = g2->GetY(); 
 int loc2 = LocMax(N2, x2);
 double _max2_ = x2[loc2];
 double _th2_ = 0.01*GetMaxGraph(g2);
 
 double _XTH_ = min(_max1_, _max2_);
 
 double *ey1 = g1->GetEY();
 
 TSpline3 s2("s2", g2); 
 
 TGraphErrors *grnew = new TGraphErrors();

 double val=0;
 int kk=0;
 do
 {
  val = s2.Eval(x1[kk]);
  grnew->SetPoint(kk, x1[kk], y1[kk] - val);
  grnew->SetPointError(kk, padSize/2., ey1[kk]); 
  kk++;
 }
 while(x1[kk]<_XTH_ && x2[kk]<_XTH_ && y1[kk]>_th1_);


 grnew->SetTitle("Absolute Residuals");
 grnew->SetName("Absolute_Residuals");
 grnew->GetXaxis()->SetTitle(g1->GetXaxis()->GetTitle());
 grnew->GetYaxis()->SetTitle("RESIDUALS");
 
 grnew->SetMarkerStyle(20);
 grnew->SetMarkerColor(kBlue);
 
 grnew->SetLineColor(kBlue);
 grnew->SetLineWidth(3);
 
 return grnew;
}

/*********************************************************************/
/*********************************************************************/

TGraphErrors *ComputeRelativeResiduals(TGraphErrors *g1, TGraph *g2)
{ 
 /*
 	g1 : exp
 	g2 : model
 */
 int N1= g1->GetN();
 double *x1 = g1->GetX();
 double *y1 = g1->GetY(); 
 double *e1 = g1->GetEY();
 
 int loc1 = LocMax(N1, x1);
 double _max1_ = x1[loc1];
 
 double _th1_ = 0.01*GetMaxGraph(g1);
 
 int N2 = g2->GetN();
 double *x2 = g2->GetX();
 double *y2 = g2->GetY(); 
 int loc2 = LocMax(N2, x2);
 double _max2_ = x2[loc2];

 double _th2_ = 0.01*GetMaxGraph(g2); 
 
 double _XTH_ = min(_max1_, _max2_);
 
 TSpline3 s2("s2", g2); 
 
 TGraphErrors *grnew = new TGraphErrors();
 
 int kk=0;
 do
 {
  double val = s2.Eval(x1[kk]);  
  
  if(y1[kk]<1e-2)
  {
   kk++;
   continue;
  }
  double rval = 100*(y1[kk] - val)/val;
  grnew->SetPoint(kk, x1[kk], rval);
  grnew->SetPointError(kk, padSize/2., 100*(e1[kk]/val));
  kk++;
 }
 while(x1[kk]<_XTH_ && x2[kk]<_XTH_ && y1[kk]>_th1_);

 grnew->SetTitle("Relative Residuals");
 grnew->SetName("Relative_Residuals");
 grnew->GetXaxis()->SetTitle(g1->GetXaxis()->GetTitle());
 grnew->GetYaxis()->SetTitle("RESIDUALS (%)");
 
 
 grnew->SetMarkerStyle(20);
 grnew->SetMarkerColor(kBlue);

 grnew->SetLineColor(kBlue);
 grnew->SetLineWidth(3);
 
 return grnew;
}


/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/

double GetChi2(TGraphErrors *calATS, TGraph *srim)
{
 int nonZeroCols = 0;
 double chi2 = 0.;
 
 double *xats = calATS->GetX();
 double *yats = calATS->GetY();
 double *ediff = calATS->GetEY();
 
 double _max_ = GetMaxGraph(calATS);
 
 int Nsrim = srim->GetN();
 double *xsrim = srim->GetX();
 
 double xlast = xsrim[Nsrim-1];
 

 TSpline3 ssrim("ssrim", srim);

 for(int kk=0; kk<NCOLS; kk++)
 {
  if(ediff[kk]!=0 && yats[kk]>_FRACTION_*_max_ && xats[kk]<=xlast)
  {
   nonZeroCols++;
   chi2 += Power(yats[kk]-ssrim.Eval(xats[kk]),2)/(ediff[kk]*ediff[kk]);
  }
  else
  	continue; 	
 }
  
 return chi2/nonZeroCols;
}

/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/

double GetChi2(TGraphErrors *ats, TGraph *srim, double cal)
{
 TGraphErrors *calATS = ScaleYErrors(ats, cal);
 double chi2 = GetChi2(calATS, srim);
 
 delete calATS;
 
 return chi2;
}

/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/

void makeElossTree(TString filename = "help", TString oname = "", TString table = "", Int_t Entries = 0, double th = 0)
{
 cout<<"****** WELCOME ********\n";

 TString helper(filename);
 helper.ToLower();
 if(helper=="help")
 {
  cout<<"****** Input Arguments:\n\n";
  cout<<"filename: path to input ROOT file (DEFAULT: show this helper)\n";
  cout<<"oname: path to output ROOT file (DEFAULT: ./analysisEloss/ElossFile_"<<integral_do<<"_"<<integral_up<<"with_gains.root)\n";
  cout<<"table: path to pulser gains (DEFAULT: ./pulser/tableOfGains/gainsTable_line.dat)\n";
  cout<<"Entries: number of entries you want to analyse (recommended for debug, DEFAULT: all entries) \n";
  cout<<"th: when filling track 2D histogram, only bin with content>th will be filled (DEFAULT: 0)\n\n";
  cout<<"****** Now closing\n";
  
  return;
 }
 
 pDSP dsp;
 pCFD cfd;
 
 if(table=="")
 {
  Warning("makeElossTree","No table with gains is specified. The file pulser/tablesOfGains/gainsTable_line.dat will be used");
  table = "./pulser/tablesOfGains/gainsTable_line.dat";
 }
 TMatrixD gains = GetGains(table, 0);
  
 TFile *input = TFile::Open(filename);
 TTree *rawEventTree = (TTree*)input->Get("rawEventTree");
 cLookupTable *lookupTable = (cLookupTable*)input->Get("lookupTable");
 
 if(oname=="")
 {
  Warning("makeElossTree", "No name given to output file. Using default path");
  oname = Form("./analysisEloss/ElossFile_%d_%d_with_gains.root",integral_do, integral_up);
 }
 
 TFile *output = new TFile(oname, "recreate");
 TTree *elossTree = new TTree("elossTree","elossTree");	
 
 int event, Npads;
 int goodness;
 double avgypads;
  
 elossTree->Branch("event", &event, "event/I"); 
 elossTree->Branch("Npads", &Npads, "Npads/I"); 
 elossTree->Branch("goodness",&goodness,"goodness/I"); 
 elossTree->Branch("avgypads",&avgypads,"avgypads/D"); 

 double charge;
 double range[10];
 double width;
 double BraggPeak[2];
 double start;
 double theta;
 
 elossTree->Branch("charge", &charge, "charge/D");
 elossTree->Branch("range", range, "range[10]/D");
 elossTree->Branch("width", &width, "width/D"); 
 elossTree->Branch("BraggPeak", BraggPeak, "BraggPeak[2]/D");  
 elossTree->Branch("start", &start, "start/D");
 elossTree->Branch("theta",&theta,"theta/D");
 
 double gcharge;
 double grange[10];
 double gwidth;
 double gBraggPeak[2];
 double gstart;
 double gtheta;
 
 elossTree->Branch("gcharge", &gcharge, "gcharge/D");
 elossTree->Branch("grange", grange, "grange[10]/D");
 elossTree->Branch("gwidth", &gwidth, "gwidth/D"); 
 elossTree->Branch("gBraggPeak", gBraggPeak, "gBraggPeak[2]/D");  
 elossTree->Branch("gstart", &gstart, "gstart/D");
 elossTree->Branch("gtheta",&gtheta,"gtheta/D");	
 
 const int het = 11; 
  
 int accumulator=0; 
 int summator=0;
 TH2F *avg_track = new TH2F("avg_track", "Average track", NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5);
 TH1D *avg_profile;

 int gaccumulator=0;
 int gsummator=0;
 TH2F *gavg_track = new TH2F("gavg_track", "Average track (with gains)", NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5);
 TH1D *gavg_profile;
 
 int nentries = Entries;
 if(nentries<=0) 
 	nentries = rawEventTree->GetEntries();
 
 int step = 10000;
 
 #if DBGLV>0
 	cout<<"**** DEBUG MODE IS ON - USING "<<DEBUGENTRIES<<" ENTRIES *****\n";
 	nentries = DEBUGENTRIES;
 	step = 1;
 	cout<<"Starting Loop\n";
 #endif
 
 for(int eve = 0; eve<nentries; eve++)
 {
  if(eve%step==0 || eve==nentries-1)
  	cout<<"\n-----> EVENT: "<<eve<<endl;
 
  goodness = 1;
  event = eve; 
  charge = 0;
  gcharge = 0;
  
  for(int jj = 0; jj<10; jj++)
  {
    range[jj] = 0;
    grange[jj] = 0;
  }
  
  width = 0;
  gwidth = 0;
  
  BraggPeak[0] = 0;
  BraggPeak[1] = 0;
  gBraggPeak[0] = 0;
  gBraggPeak[1] = 0;
  
  start = 0; 
  gstart = 0;

  TH2F *pad = BuildTrack(rawEventTree, lookupTable, eve, th);
  Npads = pad->GetEntries();
  
  if(Npads<20)
  {
   delete pad;
   goodness=-1;
   elossTree->Fill();
   continue;
  }
  
  avgypads = GetYaverage(pad);
    
  #if DBGLV>0
  	cout<<"building Bragg profile\n";
  #endif
  
  TH1D *hBragg = pad->ProjectionX(Form("hBragg_%d",eve), integral_do,integral_up);
  if(hBragg->GetEntries()<5)
  {
   delete pad;
   delete hBragg;
   goodness=-2;
   elossTree->Fill();
   continue;
  }
  
  #if DBGLV>0
  	cout<<"getting theta\n";
  #endif
  theta = GetTheta(pad, integral_do, integral_up);
  if(Abs(theta)>_THETA_CUT_)
  {
   delete pad;
   delete hBragg;
   goodness=-3;
   elossTree->Fill();
   continue;
  }
  
  /*********** END OF CUTS *********************/
  
  double s1 = hBragg->GetBinContent(het-1);
  double s2 = hBragg->GetBinContent(het+1);
  double mc = (s2-s1)/2.;
  double qc = s1 - mc*(het-1);
  hBragg->SetBinContent(het, mc*het + qc);
  
  charge = hBragg->Integral();
  
  #if DBGLV>0
  	cout<<"getting the start\n";
  #endif
  start = GetStart(hBragg);
  
 #if DBGLV>0
  	cout<<"working on the Bragg profile\n";
  #endif
  pSignal *sigBragg = Histo2Sig(hBragg);
  
  double _max_;
  int imax = sigBragg->MaxMin(&_max_, 1);
  
  BraggPeak[0] = imax*2;
  BraggPeak[1] = _max_;
  #if DBGLV>0
  	cout<<"computing ranges\n";
  #endif
  for(int ii=0; ii<10; ii++)
  	range[ii] = 2*cfd.CFD(sigBragg, (ii+1)/20., _max_, -1,1,3,0, imax) - start;
  #if DBGLV>0
  	cout<<"getting transverse straggling\n";
  #endif
  width = TransverseWidth(pad);
  
  accumulator++;
  avg_track->Add(pad,1); 
  
  delete sigBragg;
  delete hBragg;
  delete pad;
  
  /***/
  #if DBGLV>0
  	cout<<"\n************* Using gains\n\n";
  #endif
  
  TH2F *gpad = BuildTrackWithGains(rawEventTree, lookupTable, gains, eve, th);
  
  #if DBGLV>0
  	cout<<"building Bragg profile\n";
  #endif
  TH1D *ghBragg = gpad->ProjectionX(Form("ghBragg_%d",eve), integral_do,integral_up);
  
  double gs1 = ghBragg->GetBinContent(het-1);
  double gs2 = ghBragg->GetBinContent(het+1);
  double gmc = (gs2-gs1)/2.;
  double gqc = gs1 - gmc*(het-1);
  ghBragg->SetBinContent(het, gmc*het + gqc);
  
  gcharge = ghBragg->Integral();
  
  #if DBGLV>0
  	cout<<"getting the start\n";
  #endif
  gstart = GetStart(ghBragg);
  
  #if DBGLV>0
  	cout<<"working on the Bragg profile\n";
  #endif
  pSignal *gsigBragg = Histo2Sig(ghBragg);
    
  double _gmax_;
  int gimax = sigBragg->MaxMin(&_gmax_, 1);
  
  gBraggPeak[0] = gimax*2;
  gBraggPeak[1] = _gmax_;
  
  #if DBGLV>0
  	cout<<"computing ranges\n";
  #endif
  for(int ii=0; ii<10; ii++)
  	grange[ii] = 2*cfd.CFD(gsigBragg, (ii+1)/20., _gmax_, -1,1,3,0, gimax) - gstart;
  
 #if DBGLV>0
  	cout<<"getting transverse straggling\n";
  #endif
  gwidth = TransverseWidth(gpad);
  //gwidth = ProjWidth(gpad);
  #if DBGLV>0
  	cout<<"getting theta\n";
  #endif
  gtheta = GetTheta(gpad, integral_do, integral_up);
  
  gaccumulator++;
  gavg_track->Add(gpad,1); 
  
  delete gsigBragg;
  delete ghBragg;
  delete gpad;
  
  elossTree->Fill();
 }
 /* end loop on entries of rawEventTree*/
 
 avg_track->Scale(1./accumulator);
 avg_profile = avg_track->ProjectionX();
 avg_profile->SetName("avg_profile");
 avg_profile->SetTitle(Form("Average Bragg profile of %d tracks", accumulator));
 
 gavg_track->Scale(1./gaccumulator);
 gavg_profile = gavg_track->ProjectionX();
 gavg_profile->SetName("gavg_profile");
 gavg_profile->SetTitle(Form("Average Bragg profile of %d tracks (with gains)", gaccumulator));

 output->cd();
 output->Write();
 output->Close();
}


/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/



void makeElossTreeEqualizationOnly(TString filename = "help", TString oname = "", TString table = "", double vdrift =0, double delay = 0, double Ts = 1, Int_t Entries = 0, double th = 0)
{
 cout<<"****** WELCOME ********\n";

 TString helper(filename);
 helper.ToLower();
 if(helper=="help")
 {
  cout<<"****** Input Arguments:\n\n";
  cout<<"filename: path to input ROOT file (DEFAULT: show this helper)\n";
  cout<<"oname: path to output ROOT file (DEFAULT: ./analysisEloss/ElossFile_"<<integral_do<<"_"<<integral_up<<"with_gains.root)\n";
  cout<<"table: path to pulser gains (DEFAULT: ./pulser/tableOfGains/gainsTable_line.dat)\n";
  cout<<"Entries: number of entries you want to analyse (recommended for debug, DEFAULT: all entries) \n";
  cout<<"th: when filling track 2D histogram, only bin with content>th will be filled (DEFAULT: 0)\n\n";
  cout<<"****** Now closing\n";
  
  return;
 }
 const double step = padSize/100.; /* used for numeric integration */
 
 pDSP dsp;
 pCFD cfd;
 
 if(table=="")
 {
  Warning("makeElossTree","No table with gains is specified. The file pulser/tablesOfGains/gainsTable_line.dat will be used");
  table = "./pulser/tablesOfGains/gainsTable_line.dat";
 }
 TMatrixD gains = GetGains(table, 0);
  
 TFile *input = TFile::Open(filename);
 TTree *rawEventTree = (TTree*)input->Get("rawEventTree");
 cLookupTable *lookupTable = (cLookupTable*)input->Get("lookupTable");
 
 if(oname=="")
 {
  Warning("makeElossTree", "No name given to output file. Using default path");
  oname = Form("./analysisEloss/ElossFile_%d_%d_equalization_only.root",integral_do, integral_up);
 }
 
 TFile *output = new TFile(oname, "recreate");
 TTree *elossTree = new TTree("elossTree","elossTree");	
 
 int event, Npads;
 int goodness;
 double avgypads;
  
 elossTree->Branch("event", &event, "event/I"); 
 elossTree->Branch("Npads", &Npads, "Npads/I"); 
 elossTree->Branch("goodness",&goodness,"goodness/I"); 
 elossTree->Branch("avgypads",&avgypads,"avgypads/D"); 

 double charge;
 double range[10];
 double width;
 double BraggPeak[2];
 double start;
 double theta;
 double phi;
 
 elossTree->Branch("charge", &charge, "charge/D");
 elossTree->Branch("range", range, "range[10]/D");
 elossTree->Branch("width", &width, "width/D"); 
 elossTree->Branch("BraggPeak", BraggPeak, "BraggPeak[2]/D");  
 elossTree->Branch("start", &start, "start/D");
 elossTree->Branch("theta",&theta,"theta/D");
 elossTree->Branch("phi",&phi, "phi/D");
 
 int accumulator=0; 
 TH2F *avg_track = new TH2F("avg_track", "Average track", NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5);
 TH1D *avg_profile = new TH1D("avg_profile", "Average profile", NCOLS, -0.5, NCOLS-0.5);
 
 array<double, NCOLS> distanceBragg;
 array<double, NCOLS> edistance;
 
 array<double, NCOLS> S;
 array<double, NCOLS> S2;
 array<double, NCOLS> eS;
  
 int nentries = Entries;
 if(nentries<=0) 
 	nentries = rawEventTree->GetEntries();
 
 int _STEP_ = 10000;
 
 #if DBGLV>0
 	cout<<"**** DEBUG MODE IS ON - USING "<<DEBUGENTRIES<<" ENTRIES *****\n";
 	nentries = DEBUGENTRIES;
 	_STEP_  = 1;
 	cout<<"Starting Loop\n";
 #endif
 
 const int het = 11; 
 /*
 	pad (10, 17) is broken/saturated... Its value is always 0
 	-->> Bragg profiles show a dip in this point
 	linear interpolation using nearest values 
 	is used to estimate the missing value
 */
 
 for(int eve = 0; eve<nentries; eve++)
 {
  if(eve%_STEP_ ==0 || eve==nentries-1)
  	cout<<"\n-----> EVENT: "<<eve<<endl;
 
  goodness = 1;
  event = eve; 
  charge = 0;
  
  for(int jj = 0; jj<10; jj++)
  	range[jj] = 0;
  
  width = 0;
  
  BraggPeak[0] = 0;
  BraggPeak[1] = 0;
  
  start = 0; 
  theta = 0;
  phi = 0;

  TH2F *pad = BuildTrackWithGains(rawEventTree, lookupTable, gains, eve, th);
  Npads = pad->GetEntries();
  
  if(Npads<_NPADS_CUT_)
  {
   delete pad;
   goodness=-1;
   elossTree->Fill();
   continue;
  }
  
  avgypads = GetYaverage(pad);
    
  #if DBGLV>0
  	cout<<"building Bragg profile\n";
  #endif
  
  TH1D *hBragg = pad->ProjectionX(Form("hBragg_%d",eve), integral_do,integral_up);
  if(hBragg->GetEntries()<_BRAGG_ENTRIES_CUT_)
  {
   delete pad;
   delete hBragg;
   goodness=-2;
   elossTree->Fill();
   continue;
  }

 #if DBGLV>0
  	cout<<"getting theta\n";
  #endif
  theta = GetTheta(pad, integral_do, integral_up);
  if(Abs(theta)>_THETA_CUT_)
  {
   delete pad;
   delete hBragg;
   goodness=-3;
   elossTree->Fill();
   continue;
  }
  
  #if DBGLV>0
  	cout<<"getting phi\n";
  #endif
  TH2F *side = BuildSideView(rawEventTree, lookupTable, vdrift, delay, Ts, eve, th);
  phi = GetTheta(side, 0, 20);
  if(Abs(phi)>_PHI_CUT_)
  {
   delete pad;
   delete hBragg;
   delete side;
   goodness=-4;
   elossTree->Fill();
   continue;
  }
  
  /*********** END OF CUTS *********************/

  double s1 = hBragg->GetBinContent(het-1);
  double s2 = hBragg->GetBinContent(het+1);
  double mc = (s2-s1)/2.;
  double qc = s1 - mc*(het-1);
  hBragg->SetBinContent(het, mc*het + qc);
  
  charge = hBragg->Integral();
  
  #if DBGLV>0
  	cout<<"getting the start\n";
  #endif
  start = GetStart(hBragg);
  
 #if DBGLV>0
  	cout<<"working on the Bragg profile\n";
  #endif
  pSignal *sigBragg = Histo2Sig(hBragg);
  
  double _max_;
  int imax = sigBragg->MaxMin(&_max_, 1);
  
  BraggPeak[0] = imax*2;
  BraggPeak[1] = _max_;
  #if DBGLV>0
  	cout<<"computing ranges\n";
  #endif
  for(int ii=0; ii<10; ii++)
  	range[ii] = 2*cfd.CFD(sigBragg, (ii+1)/20., _max_, -1,1,3,0, imax) - start;
  #if DBGLV>0
  	cout<<"getting transverse straggling\n";
  #endif
  width = TransverseWidth(pad);
  
  /* filling arrays for average Bragg profile with errors */
  
  #if DBGLV>0
  	cout<<"prepare arrays...\n";
  #endif
  for(int ww = 0; ww<NCOLS; ww++)
  {
   double d = ww*2;
   double val = hBragg->GetBinContent(ww+1)/2.;
   distanceBragg[ww] = d;
   edistance[ww] = padSize/2.;
   #if DBGLV>0
   	cout<<val<<endl;
   #endif
   S[ww] += val;
   S2[ww] += val*val;   
  }
  
    accumulator++;
   #if DBGLV>0
  	cout<<"adding to-be-averaged 2D histogram \n";
  #endif
  avg_track->Add(pad,1); 
  #if DBGLV>0
  	cout<<"adding to-be-averaged 1D histogram \n";
  #endif
  avg_profile->Add(hBragg, 1);
  
  #if DBGLV>0
  	cout<<"almost done, doing delete\n";
  #endif
  /***** DELETE *****/
  
  delete side;
  delete sigBragg;
  delete hBragg;
  delete pad;
  
  /***** FILL *****/  
    
  elossTree->Fill();
 }
 /* end loop on entries of rawEventTree*/
  
 TGraphErrors* braggProfileErrorBars = new TGraphErrors(NCOLS);
 braggProfileErrorBars->SetTitle(Form("Bragg profile (average of %d tracks)", accumulator));
 braggProfileErrorBars->SetName("braggProfileErrorBars");
 braggProfileErrorBars->GetXaxis()->SetTitle("depth (mm)");
 braggProfileErrorBars->SetMarkerStyle(20);
 braggProfileErrorBars->SetMarkerSize(1);  
   
 for(int kk = 0; kk<NCOLS; kk++)
 {
  S[kk] /= accumulator;
  S2[kk] /= accumulator;
  eS[kk] = Sqrt(S2[kk] - S[kk]*S[kk]);
  
  braggProfileErrorBars->SetPoint(kk, distanceBragg[kk], S[kk]);
  braggProfileErrorBars->SetPointError(kk, edistance[kk], eS[kk]);
 }
  
 avg_track->Scale(1./accumulator);
 avg_profile->Scale(1./accumulator);
 avg_profile->SetTitle(Form("Average Bragg profile of %d tracks with equalization", accumulator));
 
 output->cd();
 output->Write();
 braggProfileErrorBars->Write();
 output->Close();
}

/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/

void makeElossTreeFastComparison(TString filename = "help", TString oname = "", TString table = "", TString model = "", double calib = 1., Int_t Entries = 0, double th = 0)
{
 cout<<"****** WELCOME ********\n";

 TString helper(filename);
 helper.ToLower();
 if(helper=="help")
 {
  cout<<"****** Input Arguments:\n\n";
  cout<<"filename: path to input ROOT file (DEFAULT: show this helper)\n";
  cout<<"oname: path to output ROOT file (DEFAULT: ./analysisEloss/ElossFile_"<<integral_do<<"_"<<integral_up<<"with_gains.root)\n";
  cout<<"table: path to pulser gains (DEFAULT: ./pulser/tableOfGains/gainsTable_line.dat)\n";
  cout<<"model: path to SRIM file (DEFAULT: raise an error and close the analyzer)\n";
  cout<<"calib: calibration factor (keV/ch, DEFAULT: 1, i.e. no calibration)\n\n";
  cout<<"Entries: number of entries you want to analyse (recommended for debug, DEFAULT: all entries) \n";

  cout<<"****** Now closing\n";
  
  return;
 }
 
 const double step = padSize/100.; /* used for numeric integration */
 
 TGraph *SRIM = srimBragg(model.Data());
 if(SRIM->GetN()<5)
 {
  Error("makeElossTreeFastComparison", "Model MUST be given. Check your path and file");
  return;
 }
 SRIM->SetLineColor(kGreen+2);
 SRIM->SetLineWidth(2);
 
 TSpline3 spSRIM("spSRIM", SRIM);
  
 pDSP dsp;
 pCFD cfd;
 
 if(table=="")
 {
  Warning("makeElossTree","No table with gains is specified. The file pulser/tablesOfGains/gainsTable_line.dat will be used");
  table = "./pulser/tablesOfGains/gainsTable_line.dat";
 }
 TMatrixD gains = GetGains(table, 0);
  
 TFile *input = TFile::Open(filename);
 TTree *rawEventTree = (TTree*)input->Get("rawEventTree");
 cLookupTable *lookupTable = (cLookupTable*)input->Get("lookupTable");
 
 if(oname=="")
 {
  Warning("makeElossTree", "No name given to output file. Using default path");
  oname = Form("./analysisEloss/ElossFile_%d_%d_equalization_only.root",integral_do, integral_up);
 }
 
 TFile *output = new TFile(oname, "recreate");
 TTree *elossTree = new TTree("elossTree","elossTree");	
 
 int event, Npads;
 int goodness;
 double charge;
 double range; /* @ 10% maximum ONLY */
 double width;
 double theta;
 double chisq;
  
 elossTree->Branch("event", &event, "event/I"); 
 elossTree->Branch("Npads", &Npads, "Npads/I"); 
 elossTree->Branch("goodness",&goodness,"goodness/I");  
 elossTree->Branch("charge", &charge, "charge/D");
 elossTree->Branch("range", &range, "range/D");
 elossTree->Branch("width", &width, "width/D"); 
 elossTree->Branch("theta",&theta,"theta/D");
 elossTree->Branch("chisq",&chisq, "chisq/D");
 
 int accumulator=0; 
 TH2F *avg_track = new TH2F("avg_track", "Average track", NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5);
 TH1D *avg_profile = new TH1D("avg_profile", "Average profile", NCOLS, -0.5, NCOLS-0.5);
 
 array<double, NCOLS> distanceBragg;
 array<double, NCOLS> edistance;
 array<double, NCOLS> S;
 array<double, NCOLS> S2;
 array<double, NCOLS> eS;
  
 int nentries = Entries;
 if(nentries<=0) 
 	nentries = rawEventTree->GetEntries();
 
 int _STEP_ = 10000;
 
 #if DBGLV>0
 	cout<<"**** DEBUG MODE IS ON - USING "<<DEBUGENTRIES<<" ENTRIES *****\n";
 	nentries = DEBUGENTRIES;
 	_STEP_  = 1;
 	cout<<"Starting Loop\n";
 #endif
 
 const int het = 11; 
 /*
 	pad (10, 17) is broken/saturated... Its value is always 0
 	-->> Bragg profiles show a dip in this point
 	linear interpolation using nearest values 
 	is used to estimate the missing value
 */
 
 for(int eve = 0; eve<nentries; eve++)
 {
  if(eve%_STEP_ ==0 || eve==nentries-1)
  	cout<<"\n-----> EVENT: "<<eve<<endl;
 
  goodness = 1;
  event = eve; 
  charge = 0;
  range = 0;
  width = 0;
  theta = 0;
  chisq = 0;

  TH2F *pad = BuildTrackWithGains(rawEventTree, lookupTable, gains, eve, th);
  Npads = pad->GetEntries();
  
  if(Npads<_NPADS_CUT_)
  {
   delete pad;
   goodness=-1;
   elossTree->Fill();
   continue;
  }
    
  #if DBGLV>0
  	cout<<"building Bragg profile\n";
  #endif
  
  TH1D *hBragg = pad->ProjectionX(Form("hBragg_%d",eve), integral_do,integral_up);
  if(hBragg->GetEntries()<_BRAGG_ENTRIES_CUT_)
  {
   delete pad;
   delete hBragg;
   goodness=-2;
   elossTree->Fill();
   continue;
  }

 #if DBGLV>0
  	cout<<"getting theta\n";
  #endif
  theta = GetTheta(pad, integral_do, integral_up);
  if(Abs(theta)>_THETA_CUT_)
  {
   delete pad;
   delete hBragg;
   goodness=-3;
   elossTree->Fill();
   continue;
  }
  
  /*********** END OF CUTS *********************/

  double s1 = hBragg->GetBinContent(het-1);
  double s2 = hBragg->GetBinContent(het+1);
  double mc = (s2-s1)/2.;
  double qc = s1 - mc*(het-1);
  hBragg->SetBinContent(het, mc*het + qc);
  
  charge = hBragg->Integral();
    
 #if DBGLV>0
  	cout<<"working on the Bragg profile\n";
  #endif
  pSignal *sigBragg = Histo2Sig(hBragg);
  
  double _max_;
  int imax = sigBragg->MaxMin(&_max_, 1);
  
  double start = GetStart(hBragg);

  #if DBGLV>0
  	cout<<"computing range\n";
  #endif
  	range = 2*cfd.CFD(sigBragg, 0.1, _max_, -1,1,3,0, imax) - start;

  #if DBGLV>0
  	cout<<"getting transverse straggling\n";
  #endif
  width = TransverseWidth(pad);
  
  /******/
  
  TGraph *eventATS = ATS2Physics(hBragg, 0.5*calib);
  
  double *xev = eventATS->GetX();  
  double *yev = eventATS->GetY();  
  for(int kk=0; kk<NCOLS; kk++)
  {
   double val = spSRIM.Eval(xev[kk])-yev[kk];
   chisq += val*val/NCOLS;
  }
  /* filling arrays for average Bragg profile with errors */
  
  #if DBGLV>0
  	cout<<"prepare arrays...\n";
  #endif
  for(int ww = 0; ww<NCOLS; ww++)
  {
   double d = ww*2;
   double val = hBragg->GetBinContent(ww+1)/2.;
   distanceBragg[ww] = d;
   edistance[ww] = padSize/2.;
   #if DBGLV>0
   	cout<<val<<endl;
   #endif
   S[ww] += val;
   S2[ww] += val*val;   
  }
  
  accumulator++;
  #if DBGLV>0
  	cout<<"adding to-be-averaged 2D histogram \n";
  #endif
  avg_track->Add(pad,1); 
  #if DBGLV>0
  	cout<<"adding to-be-averaged 1D histogram \n";
  #endif
  avg_profile->Add(hBragg, 1);
  
  #if DBGLV>0
  	cout<<"almost done, doing delete\n";
  #endif
  
  /***** DELETE *****/
  
  delete eventATS;
  delete sigBragg;
  delete hBragg;
  delete pad;
  
  /***** FILL *****/  
    
  elossTree->Fill();
 }
 /* end loop on entries of rawEventTree*/
  
 TGraphErrors* braggProfileErrorBars = new TGraphErrors(NCOLS);
 braggProfileErrorBars->SetTitle(Form("Bragg profile (average of %d tracks)", accumulator));
 braggProfileErrorBars->SetName("braggProfileErrorBars");
 if(calib!=1)
	 braggProfileErrorBars->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)"); 
 braggProfileErrorBars->GetXaxis()->SetTitle("depth (mm)");
 braggProfileErrorBars->SetMarkerStyle(20);
 braggProfileErrorBars->SetMarkerColor(kRed+1);
 braggProfileErrorBars->SetMarkerSize(1);  
   
 for(int kk = 0; kk<NCOLS; kk++)
 {
  S[kk] /= accumulator;
  S2[kk] /= accumulator;
  eS[kk] = Sqrt(S2[kk] - S[kk]*S[kk]);
  
  braggProfileErrorBars->SetPoint(kk, distanceBragg[kk], calib*S[kk]);
  braggProfileErrorBars->SetPointError(kk, edistance[kk], calib*eS[kk]);
 }
  
 avg_track->Scale(1./accumulator);
 avg_profile->Scale(1./accumulator);
 avg_profile->SetTitle(Form("Average Bragg profile of %d tracks with equalization", accumulator));
 
 output->cd();
 output->Write();
 braggProfileErrorBars->Write();
 SRIM->Write("srim");
 output->Close();
}

/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/

void makeElossTreeWithProfiles(TString filename = "help", TString oname = "", TString table = "", double vdrift =0, double delay = 0, double Ts = 1, Int_t Entries = 0, double th = 0)
{
 TString helper(filename);
 helper.ToLower();
 if(helper=="help")
 {
  cout<<"****** Input Arguments:\n\n";
  
  cout<<"filename: path to input ROOT file (DEFAULT: show this helper)\n";
  cout<<"oname: path to output ROOT file (DEFAULT: ./analysisEloss/ElossFile_"<<integral_do<<"_"<<integral_up<<"with_gains.root)\n";
  cout<<"table: path to pulser gains (DEFAULT: ./pulser/tableOfGains/gainsTable_line.dat)\n";
  cout<<"Entries: number of entries you want to analyse (recommended for debug, DEFAULT: all entries) \n";

  cout<<"****** Now closing\n";
  
  return;
 }
   
 pDSP dsp;
 pCFD cfd;
 
 if(table=="")
 {
  Warning("makeElossTreeWithProfiles","No table with gains is specified. The file pulser/tablesOfGains/gainsTable_line.dat will be used");
  table = "./pulser/tablesOfGains/gainsTable_line.dat";
 }
 TMatrixD gains = GetGains(table, 0);
  
 TFile *input = TFile::Open(filename);
 TTree *rawEventTree = (TTree*)input->Get("rawEventTree");
 cLookupTable *lookupTable = (cLookupTable*)input->Get("lookupTable");
 
 if(oname=="")
 {
  Warning("makeElossTreeWithProfiles", "No name given to output file. Using default path");
  oname = Form("./analysisEloss/ElossFile_%d_%d_equalization_only.root",integral_do, integral_up);
 }
 
 TFile *output = new TFile(oname, "recreate");
 TTree *elossTree = new TTree("elossTree","elossTree");	
 
 int event, Npads;
 int goodness;
 double chargeTH2,charge;
 double range005, range010, range050; 
 double width;
 double theta;
 double phi;
 
 TH2F *evTrack = new TH2F("evTrack","evTrack", NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5);
 	     evTrack->GetXaxis()->SetTitle("COLS");
	     evTrack->GetYaxis()->SetTitle("ROWS");

 /*  
 TH2F *evSide = new TH2F("evSide","evSide", NCOLS, -0.5*padSize, padSize*(NCOLS-0.5), HEIGHT, -0.5, HEIGHT-0.5);
 	     evSide->GetXaxis()->SetTitle("x (mm)");
	     evSide->GetYaxis()->SetTitle("z (mm)");
 */	
 
 TGraph *evProfile = new TGraph(NCOLS);
	     evProfile->SetMarkerStyle(20);
	     evProfile->SetMarkerSize(1);
	     evProfile->GetXaxis()->SetTitle("depth (mm)");
	     evProfile->GetYaxis()->SetTitle("#frac{dQ}{dx}");
	     
 TGraph *evDiffusion = new TGraph(NCOLS);
	     evDiffusion->SetMarkerStyle(20);
	     evDiffusion->SetMarkerSize(1);
	     evDiffusion->GetXaxis()->SetTitle("depth (mm)");
	     evDiffusion->GetYaxis()->SetTitle("#sigma (mm)");
  
 elossTree->Branch("event", &event, "event/I"); 
 elossTree->Branch("Npads", &Npads, "Npads/I"); 
 elossTree->Branch("goodness",&goodness,"goodness/I");  
 elossTree->Branch("charge", &charge, "charge/D");
 elossTree->Branch("range005", &range005, "range005/D");
 elossTree->Branch("range010", &range010, "range010/D");
 elossTree->Branch("range050", &range050, "range050/D");
 elossTree->Branch("width", &width, "width/D"); 
 elossTree->Branch("evTrack", evTrack);
 elossTree->Branch("evProfile", evProfile);
 elossTree->Branch("evDiffusion", evDiffusion);
 //elossTree->Branch("theta",&theta,"theta/D");
 //elossTree->Branch("phi",&phi, "phi/D");
 //elossTree->Branch("evSide", evSide);

   
 int nentries = Entries;
 if(nentries<=0) 
 	nentries = rawEventTree->GetEntries();
 
 int _STEP_ = _COUT_STEP_;
 
 #if DBGLV>0
 	cout<<"**** DEBUG MODE IS ON - USING "<<DEBUGENTRIES<<" ENTRIES *****\n";
 	nentries = DEBUGENTRIES;
 	_STEP_  = 1;
 	cout<<"Starting Loop\n";
 #endif
 
 const int hetx1 = 11;
 const int hety1 = 18;
 const int hetx2 = 49;
 const int hety2 = 19;
 const int hetx3 = 15;
 const int hety3 = 16; 
 /*
 	pad (10, 17) (48, 18) (14, 15) are broken/saturated... 
 	Their value is always 0
 	-->> Bragg profiles show a dip in these points
 	linear interpolation using nearest pads 
 	is performed to estimate the corresponding missing values
 */
   
 cout<<"\n***** Total events to be analyzed: "<<nentries<<endl;
 
 for(int eve = 0; eve<nentries; eve++)
 {
  if(eve%_STEP_ ==0 || eve==nentries-1)
  	cout<<"\n------------------------------> EVENT: "<<eve<<endl;
 
  goodness = 1;
  event = eve; 
  charge = -100;
  range005 = -100;
  range010 = -100;
  range050 = -100;
  width = -100;
  //theta = -100;
  //phi = -100;
  evTrack->Reset();
  //evSide->Reset();
  
  #if DBGLV>0
  	cout<<"building 2D track\n";
  #endif
  TH2F *pad = BuildTrackWithGains(rawEventTree, lookupTable, gains, eve, th);
  
  int idbinL1 = pad->GetBin(hetx1-1, hety1);
  double cbinL1 = pad->GetBinContent(idbinL1);
  int idbinR1 = pad->GetBin(hetx1+1, hety1);
  double cbinR1 = pad->GetBinContent(idbinR1);  
  double cbin1 = (cbinL1+cbinR1)/2.;
  pad->SetBinContent(hetx1, hety1, cbin1);
  
  int idbinL2 = pad->GetBin(hetx2-1, hety2);
  double cbinL2 = pad->GetBinContent(idbinL2);
  int idbinR2 = pad->GetBin(hetx2+1, hety2);
  double cbinR2 = pad->GetBinContent(idbinR2);  
  double cbin2 = (cbinL2+cbinR2)/2.;
  pad->SetBinContent(hetx2, hety2, cbin2);
  
  int idbinL3 = pad->GetBin(hetx3-1, hety3);
  double cbinL3 = pad->GetBinContent(idbinL3);
  int idbinR3 = pad->GetBin(hetx3+1, hety3);
  double cbinR3 = pad->GetBinContent(idbinR3);  
  double cbin3 = (cbinL3+cbinR3)/2.;
  pad->SetBinContent(hetx3, hety3, cbin3);
  
  Npads = pad->GetEntries();
  
  for(int yy = 1; yy<=NROWS; yy++)
  {
   for(int xx = 1; xx<=NCOLS; xx++)
   {
    int idbin = pad->GetBin(xx,yy);
    double cbin = pad->GetBinContent(idbin);
    if(cbin>0)
    	evTrack->SetBinContent(xx,yy, cbin);
   }
  }
  
  evTrack->SetName(Form("evTrack_%d",eve));
  evTrack->SetTitle(Form("evTrack_%d",eve));
  
  #if DBGLV>0
  	cout<<"building 1D track\n";
  #endif
  TH1D *hBragg = pad->ProjectionX(Form("hBragg_%d",eve), integral_do,integral_up);
    
  /*********** START OF CUTS *********************/
    
  bool _MINIMAL_CUTS_ = (hBragg->GetEntries()>_BRAGG_ENTRIES_CUT_) && (Npads>_NPADS_CUT_);
  
  if(!_MINIMAL_CUTS_)
  {
   delete pad;
   delete hBragg;
   goodness=-1;
   elossTree->Fill();
   continue;
  }
  
  /*********** END OF CUTS *********************/
  /*
  TH2F *side = BuildSideView(rawEventTree, lookupTable, vdrift, delay, Ts, eve, th);
    
  for(int zz=1; zz<=side->GetNbinsY(); zz++)
  {
   for(int xx=1; xx<=NCOLS; xx++)
   {
    int idbin = side->GetBin(xx,zz);
    double cbin = side->GetBinContent(idbin);
    if(cbin>0)
    	evSide->SetBinContent(xx,zz, cbin);   
   }
  }

  evSide->SetName(Form("evSide_%d",eve));
  evSide->SetTitle(Form("evSide_%d",eve));
  */
  
  #if DBGLV>0
  	cout<<"pad --> mm\n";
  #endif
  /*
  TGraph* profileTemp = ATS2Physics(hBragg, 0.5);
  profileTemp->SetName(Form("profile_%d",eve));
  profileTemp->SetTitle(Form("profile_%d",eve));
  */
  
  //double *yprofileTemp = profileTemp->GetY();
  for(int xx=0; xx<NCOLS; xx++)
  	evProfile->SetPoint(xx, 1 + (padSize+padPitch)*xx, hBragg->GetBinContent(xx+1)/2.);
  evProfile->SetName(Form("evProfile_%d",eve));
  
  charge = IntegrateProfile(evProfile, 0,130);

  TGraph* diffusionTemp = DiffusionWidth(pad);
  diffusionTemp->SetName(Form("diffusion_%d",eve));
  diffusionTemp->SetTitle(Form("diffusion_%d",eve));
  
  double *ydiffusionTemp = diffusionTemp->GetY();
  for(int xx=0; xx<NCOLS; xx++)
  	evDiffusion->SetPoint(xx, 1 + (padSize+padPitch)*xx, ydiffusionTemp[xx]);
  evDiffusion->SetName(Form("evDiffusion_%d",eve));
  /*
  #if DBGLV>0
  	cout<<"getting theta\n";
  #endif
  //theta = GetTheta(pad, integral_do, integral_up);
  
  #if DBGLV>0
  	cout<<"getting phi\n";
  #endif
  //phi = GetPhi(side, 0, 100);
  */
    
  #if DBGLV>0
  	cout<<"working on the Bragg profile\n";
  #endif
  

  #if DBGLV>0
  	cout<<"computing range(s)\n";
  #endif
  double start = GetStart(hBragg);
  
  range005 = GetRangeGraph(evProfile, 0.05) - start;
  range010 = GetRangeGraph(evProfile, 0.10) - start;
  range050 = GetRangeGraph(evProfile, 0.50) - start;
  
  /*
  pSignal *sigBragg = Histo2Sig(hBragg);
  
  double _max_;
  int imax = sigBragg->MaxMin(&_max_, 1);
   
  

  range005 = (padSize+padPitch)*cfd.CFD(sigBragg, 0.05, _max_, -1,1,3,0, imax) - start;
  range010 = (padSize+padPitch)*cfd.CFD(sigBragg, 0.1, _max_, -1,1,3,0, imax) - start;
  range050 = (padSize+padPitch)*cfd.CFD(sigBragg, 0.5, _max_, -1,1,3,0, imax) - start;
  	
  delete sigBragg;
 */

  #if DBGLV>0
  	cout<<"getting transverse width\n";
  #endif
  width = TransverseWidth(pad);
  
  /***** DELETE *****/
  
  //delete side;

  delete pad;
  delete hBragg;
  delete diffusionTemp;
  
  //yprofileTemp = NULL;
  //xdiffusionTemp = NULL;
  ydiffusionTemp = NULL;

  /***** FILL *****/  
     
  elossTree->Fill();
 }
 /* end loop on entries of rawEventTree*/
 
 TNamed *info = new TNamed("***** USEFUL INFO *****", Form("vdrift = %1.4f mm/ns, Ts = %2.1f ns", vdrift, Ts));
 
 output->cd();
 info->Write();
 elossTree->Write();
 output->Close();
}


/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/

			/* 
				calibration and plots
				cuts can be given
				N.B. : tracks MUST be saved in the TTree
			*/
void StoreResults(TString elossFile, TString results, TString modelSRIM, TString modelLISE, double sigma, double tail=0, double front=200, int firstEntry = -1, int lastEntry=-1, double step=padSize/100.)
{ 
 TFile *_file_ = TFile::Open(elossFile);
 TTree *elossTree = (TTree*)_file_->Get("elossTree");
 if(elossTree==NULL)
 {
  Fatal("ShowResults","Pointer to TTree is NULL");
  return;  
 }
 TGraph *srim = Diffuse(modelSRIM.Data(), sigma);
 if(srim->GetN()<5)
 {
  Fatal("ShowResults","No TRIM model is given");
  return;
 }
 double srim050 = GetRangeGraph(srim, 0.5);
 double srim005 = GetRangeGraph(srim, 0.05); 
 double srim010 = GetRangeGraph(srim, 0.1);
 
 TGraph *lise = srimBragg(modelLISE.Data());//Diffuse(modelLISE.Data(), sigma);
 if(lise->GetN()<5)
 {
  Fatal("ShowResults","No LISE model is given");
  return;
 }
 double lise050 = GetRangeGraph(lise, 0.5);
 double lise005 = GetRangeGraph(lise, 0.05); 
 double lise010 = GetRangeGraph(lise, 0.1);
  
 TTreeReader elossReader(elossTree);
 
 TTreeReaderValue<int> eventValue(elossReader, "event"); 

 TTreeReaderValue<double> chargeValue(elossReader, "charge");
 TTreeReaderValue<double> range050Value(elossReader, "range050");
 TTreeReaderValue<double> widthValue(elossReader, "width");
 
 TTreeReaderValue<int> NpadsValue(elossReader, "Npads");
 
 TTreeReaderValue<double> range010Value(elossReader, "range010");
 TTreeReaderValue<double> range005Value(elossReader, "range005");
 
 //TTreeReaderValue<double> thetaValue(elossReader, "theta");
 //TTreeReaderValue<double> phiValue(elossReader, "phi");
 
 TTreeReaderValue<TH2F> evTrackValue(elossReader, "evTrack");   
 TTreeReaderValue<TGraph> evProfileValue(elossReader, "evProfile");
 TTreeReaderValue<TGraph> evDiffusionValue(elossReader, "evDiffusion");
 
 const double dQdxMax = 5000.;
 TH1D *oneColumnProfile[NCOLS];
 TF1 *gausProfile = new TF1("gausProfile","gaus(0)",0.,dQdxMax);
 gausProfile->SetParLimits(1, 0, dQdxMax);
 gausProfile->SetParLimits(2, 0, dQdxMax);
 
 TH1D *oneColumnDiffusion[NCOLS];
 TF1 *gausDiffusion = new TF1("gausDiffusion","gaus(0)",0,10);
 
 TH1D *oneColumnPosition[NCOLS];
 TF1 *gausPosition = new TF1("gausPosition","gaus(0)",0,64);
 
 for(int hh=0; hh<NCOLS; hh++)
 {
  oneColumnProfile[hh] = new TH1D(Form("oneColumnProfile_%d",hh),Form("oneColumnProfile_%d",hh), dQdxMax/5, 0., dQdxMax);
  oneColumnDiffusion[hh] = new TH1D(Form("oneColumnDiffusion_%d",hh),Form("oneColumnDiffusion_%d",hh), 1000,0,10);  
  oneColumnPosition[hh] = new TH1D(Form("oneColumnPosition_%d",hh),Form("oneColumnPosition_%d",hh), 1000,0,64);
 }
 
 int accumulator=0; 
 
 TH1F *totalCharge = new TH1F("hCharge","Charge distribution",5000,0,500000);
 TH1F *totalRange050 = new TH1F("hRange050","Range distribution (50%)",200,0,200);
 TH1I *totalNpads = new TH1I("hNpads","Npads distribution",1000,0,1000); 
 //TH1F *totalTheta = new TH1F("hTheta","Theta distribution",1000, -50, 50);
 //TH1F *totalPhi = new TH1F("hPhi","Theta distribution",1000, -50, 50);
 
 TH1F *totalRange005 = new TH1F("hRange005","Range distribution (5%)",200,0,200);
 
 TH2F *avgTrack = new TH2F("avgTrack","avgTrack", NCOLS, -0.5, NCOLS-0.5, NROWS, -0.5, NROWS-0.5); 
 
 TGraphErrors *maxPos = new TGraphErrors(); 
 TGraphErrors *tempProfile = new TGraphErrors(); 
 TGraphErrors *diffusion = new TGraphErrors();
 
 /* 
 	fill of histograms WITHOUT event selection
 */
 if(firstEntry<=0)
  	firstEntry = 0;
 if(lastEntry<=0)
  	lastEntry = elossTree->GetEntries();
  	
  	 cout<<"\tfirstEntry: "<<firstEntry<<"  lastEntry: "<<lastEntry<<endl;
 
 while(elossReader.Next())
 {
  if(elossReader.GetCurrentEntry()<=firstEntry || elossReader.GetCurrentEntry()>=lastEntry) 
  	continue;	
  
  totalCharge->Fill(chargeValue.Get()[0]);
  totalRange050->Fill(range050Value.Get()[0]);	
  totalNpads->Fill(NpadsValue.Get()[0]);
  //totalTheta->Fill(thetaValue.Get()[0]);
  //totalPhi->Fill(phiValue.Get()[0]);
    
  totalRange005->Fill(range005Value.Get()[0]);
 }
 /* end elossReader filling histograms */
 
 /* EVENT SELECTION - SETTING CUTS VALUE - START */

 TF1 *gausCharge = new TF1("gausCharge","gaus(0)",0,500000);
 gausCharge->SetParameter(0,  totalCharge->GetMaximum()); 
 gausCharge->SetParameter(1, totalCharge->GetMean());
 
 totalCharge->Fit("gausCharge","RNQ");

 double meanCharge = gausCharge->GetParameter(1); 
 double sigmaCharge = gausCharge->GetParameter(2);
 
 /* cut in range (50%)*/
  
 TF1 *gausRange = new TF1("gausRange","gaus(0)",0,200);
 gausRange->SetParameter(0, totalRange050->GetMaximum()); 
 gausRange->SetParameter(1, totalRange050->GetMean());
 
 totalRange050->Fit("gausRange","RNQ");

 double meanRange = gausRange->GetParameter(1); 
 double sigmaRange = gausRange->GetParameter(2);
 
 /* cut in Npads */
  
 TF1 *gausNpads = new TF1("gausNpads","gaus(0)",0,1000);
 gausNpads->SetParameter(0, totalNpads->GetMaximum()); 
 gausNpads->SetParameter(1, totalNpads->GetMean());
 
 totalNpads->Fit("gausNpads","RNQ");

 double meanNpads = gausNpads->GetParameter(1); 
 double sigmaNpads = gausNpads->GetParameter(2);

 /* cut in theta */
 /* 
 TF1 *gausTheta = new TF1("gausTheta","gaus(0)",-50, 50);
 gausTheta->SetParameter(0, totalTheta->GetMaximum()); 
 gausTheta->SetParameter(1, totalTheta->GetMean());
 
 totalTheta->Fit("gausTheta","RNQ");

 double meanTheta = gausTheta->GetParameter(1); 
 double sigmaTheta = gausTheta->GetParameter(2);
 */
 /* cut in phi */
 /*
 TF1 *gausPhi = new TF1("gausPhi","gaus(0)",-50, 50);
 gausPhi->SetParameter(0, totalPhi->GetMaximum()); 
 gausPhi->SetParameter(1, totalPhi->GetMean());
 
 totalPhi->Fit("gausPhi","RNQ");

 double meanPhi = gausPhi->GetParameter(1); 
 double sigmaPhi = gausPhi->GetParameter(2);
 */

 /* EVENT SELECTION - SETTING CUTS VALUE - END */
 
 TFile *Results = new TFile(results, "recreate");
 
 // TTree with data of the selected events
 TTree *data = new TTree("data","data");
 
 int event;
 double charge, range050, width;
 int Npads;
 double range010, range005;
 //double theta, phi;
 double peak;
 
 double good_pads, good_range, good_charge, good;
 
 data->Branch("event", &event, "event/I");
 
 data->Branch("charge", &charge, "charge/D");
 data->Branch("range050", &range050, "range050/D");
 data->Branch("Npads", &Npads, "Npads/I"); 
 //data->Branch("theta", &theta, "theta/D");
 //data->Branch("phi", &phi, "phi/D");
 
 data->Branch("width", &width, "width/D");
 data->Branch("range010", &range010, "range010/D");
 data->Branch("range005", &range005, "range005/D");

 data->Branch("peak", &peak, "peak/D");

 data->Branch("good_charge", &good_charge, "good_charge/D");
 data->Branch("good_range",  &good_range,  "good_range/D");
 data->Branch("good_pads",   &good_pads,   "good_pads/D");
 
 data->Branch("good", &good, "good/D");

 elossReader.Restart();     
 while(elossReader.Next())
 {
  if(elossReader.GetCurrentEntry()<firstEntry || elossReader.GetCurrentEntry()>lastEntry) 
  	continue;	
  	
  
  event = eventValue.Get()[0];
  
  charge = chargeValue.Get()[0]; 
  range050 = range050Value.Get()[0];
  Npads = NpadsValue.Get()[0];
  //theta = thetaValue.Get()[0];
  //phi = phiValue.Get()[0];
  
  good_pads   = Abs(Npads-meanNpads)<sigmaNpads*nsigmaNpads;
  good_range  = Abs(range050-meanRange)<sigmaRange*nsigmaRange;
  good_charge = Abs(charge-meanCharge)<sigmaCharge*nsigmaCharge;
  
  bool selection = good_pads && good_range && good_charge;
  	         /*
  	         &&
  	         Abs(theta-meanTheta)<sigmaTheta*nsigmaTheta &&
  	         Abs(phi-meanPhi)<sigmaPhi*nsigmaPhi;
  	         */
  	         
  //cout<<"__HERE__ 1\n\n";	    
  	         
  width = widthValue.Get()[0];

  range010 = range010Value.Get()[0];
  range005 = range005Value.Get()[0];
    
  if(!selection)
  {   
   good = -1;   
   data->Fill();
   continue;
  }
  
  good=1;
  
  //cout<<"__HERE__ 2\n\n";
  
  accumulator++;
  
  auto track = evTrackValue.Get()[0];
  
  avgTrack->Add(&track, 1.);
  
  for(int xx=1; xx<=NCOLS; xx++)
  {
   double _hereMax_=0;
   double _tempYY_=0;
   for(int yy=1; yy<=NROWS; yy++)
   {
    int idbin = track.GetBin(xx,yy);
    double cbin = track.GetBinContent(idbin);
    if(cbin>_hereMax_)
    {
     _hereMax_=cbin;
     _tempYY_=yy*2;
    } 
   }
   oneColumnPosition[xx-1]->Fill(_tempYY_); 
  }
  
  //cout<<"__HERE__ 3\n\n";
  
  auto profile = evProfileValue.Get()[0];  
  
  peak = GetMaxGraph(&profile);
  
  double *xprofile = profile.GetX(); 
  double *yprofile = profile.GetY(); 
  
  auto diff = evDiffusionValue.Get()[0];  
  double *ydiff = diff.GetY(); 
  
  //TGraph tempBragg(profile.GetN()); 
 
  //cout<<"__HERE__ 4\n\n";
  for(int ii=0; ii<NCOLS; ii++)
  {
   if(yprofile[ii]>0.)
   {
	   oneColumnProfile[ii]->Fill(yprofile[ii]);
	  }
	  /*
   if(xprofile[ii]>=tail && xprofile[ii]<=front)
   	tempBragg.SetPoint(ii, xprofile[ii]+padSize/2., yprofile[ii]);
   else 
   	tempBragg.SetPoint(ii, xprofile[ii]+padSize/2., 0.);
  */
   oneColumnDiffusion[ii]->Fill(ydiff[ii]);
  }
  
 //cal = ComputeCalibration(&tempBragg, srim);
 
   
 data->Fill();
 }
 /* end elossReader event selection */
 
 TF1 rangeFit("rangeFit","gaus(0)", 0,200);
 totalRange005->Fit("rangeFit","RNQ");
 double endProfile = rangeFit.GetParameter(1)+padSize;
 
 TGraph *countsBragg = new TGraph();
 
 for(int kk=0; kk<NCOLS; kk++)
 {
  double distanceBragg = (padSize+padPitch)*kk+padSize/2.;
  
  if(distanceBragg>=tail && distanceBragg<=front)
  {   
   oneColumnPosition[kk]->Fit("gausPosition","RNQ");  
   maxPos->SetPoint(kk, distanceBragg, gausPosition->GetParameter(1));
   maxPos->SetPointError(kk, padSize/2., gausPosition->GetParameter(2));
 
   oneColumnDiffusion[kk]->Fit("gausDiffusion","RNQ");
   diffusion->SetPoint(kk, distanceBragg, gausDiffusion->GetParameter(1));
   diffusion->SetPointError(kk, padSize/2., gausDiffusion->GetParameter(2));
  }
  else
  {
   maxPos->SetPoint(kk, distanceBragg,0);
   maxPos->SetPointError(kk, padSize/2.,0); 
   diffusion->SetPoint(kk, distanceBragg,0);
   diffusion->SetPointError(kk, padSize/2.,0);
  }
  

  if(distanceBragg<endProfile)
  {
   //gausProfile->SetParameter(1, oneColumnProfile[kk]->GetMean());
   oneColumnProfile[kk]->Fit("gausProfile","RNQ");
   
   double _mu_ = gausProfile->GetParameter(1);  
   double _sigma_ = gausProfile->GetParameter(2);
   
   int _kk_Entries_ = oneColumnProfile[kk]->GetEntries();
   
   countsBragg->SetPoint(kk, distanceBragg, _kk_Entries_);
  
   tempProfile->SetPoint(kk, distanceBragg, _mu_);
   tempProfile->SetPointError(kk, padSize/2., _sigma_/Sqrt(_kk_Entries_)); // ___ THE SIGMAS ARE HERE ____
  }
  else
  {
   tempProfile->SetPoint(kk, distanceBragg, 0);
   tempProfile->SetPointError(kk, padSize/2., 0);
   
   countsBragg->SetPoint(kk, distanceBragg, 0.);
  }
 }
 /*end of AVERAGE */
 
 double *xLise = lise->GetX();

 double Isrim = IntegrateProfile(srim, 0, GetRangeGraph(srim, 1e-6));
 double Ilise = IntegrateProfile(lise, 0, xLise[lise->GetN()-1]);
 
 TGraphErrors *braggProfile = new TGraphErrors(*tempProfile);
 
 TGraph *erelProfile = new TGraph();
 
 double *xavg = braggProfile->GetX();
 double *yavg = braggProfile->GetY();
 double *eyavg = braggProfile->GetEY();
 
 for(int kk=0; kk<braggProfile->GetN(); kk++)
 {
  if(yavg[kk]!=0)
  	erelProfile->SetPoint(kk, xavg[kk], 100*eyavg[kk]/yavg[kk]);
 }
 
 
 //********* SETTING FANCY STYLE AND NAME OF GRAPHS AND SAVING INTO A FILE *********"
   
 avgTrack->Scale(1./accumulator);
 avgTrack->SetTitle(Form("Average of %d (2D) tracks", accumulator));
 avgTrack->GetXaxis()->SetTitle("Column");
 avgTrack->GetYaxis()->SetTitle("Row");
  
 maxPos->SetName("maxPos");
 maxPos->SetTitle("Y-Position of Maximum vs. Depth");
 maxPos->SetMarkerStyle(45);
 maxPos->SetMarkerSize(1.4);
 maxPos->GetXaxis()->SetTitle("depth (mm)");
 maxPos->GetYaxis()->SetTitle("y-position (mm)");
 
 diffusion->SetName("Diffusion");
 diffusion->SetTitle("Transverse Diffusion vs. Depth");
 diffusion->SetMarkerStyle(20);
 diffusion->SetMarkerSize(1);
 diffusion->GetXaxis()->SetTitle("depth (mm)");
 diffusion->GetYaxis()->SetTitle("#sigma (mm)");
 
 srim->SetName("trim");
 srim->SetTitle("Bragg Profile (TRIM)");
 srim->SetMarkerStyle(21);
 srim->SetMarkerSize(1.2);
 srim->SetMarkerColor(kGreen+3);
 srim->SetLineWidth(3);
 srim->SetLineColor(kGreen);
 srim->GetXaxis()->SetTitle("depth (mm)");
 srim->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
 
 lise->SetName("lise");
 lise->SetTitle("Bragg Profile (LISE++)");
 lise->SetMarkerStyle(22);
 lise->SetMarkerSize(1.2);
 lise->SetMarkerColor(kBlue+2);
 lise->SetLineWidth(3);
 lise->SetLineColor(kBlue);
 lise->GetXaxis()->SetTitle("depth (mm)");
 lise->GetYaxis()->SetTitle("#frac{dE}{dx} (keV/mm)");
  
 braggProfile->SetName("braggProfile");
 braggProfile->SetTitle("Bragg Profile (no calib.)");
 braggProfile->SetMarkerColor(kBlack);
 braggProfile->SetLineColor(kBlack);
 braggProfile->SetMarkerStyle(20);
 braggProfile->SetMarkerSize(1.2);
 braggProfile->SetLineWidth(3);
 braggProfile->GetXaxis()->SetTitle("depth (mm)");
 braggProfile->GetYaxis()->SetTitle("#frac{dQ}{dx} (a.u./mm)");
 braggProfile->GetYaxis()->SetTitleOffset(1.1);
 
 
 erelProfile->SetName("erelProfile");
 erelProfile->SetTitle("Relative Errors of Bragg Profile");
 erelProfile->SetMarkerColor(kMagenta+3);
 erelProfile->SetLineColor(kMagenta+3);
 erelProfile->SetMarkerStyle(33);
 erelProfile->SetMarkerSize(1.2);
 erelProfile->SetLineWidth(3);
 erelProfile->GetXaxis()->SetTitle("depth (mm)");
 erelProfile->GetYaxis()->SetTitle("#varepsilon_{R} (%)");
 erelProfile->GetYaxis()->SetTitleOffset(1.1);
 

 countsBragg->SetName("countsBragg");
 countsBragg->SetTitle("Track counts of Bragg Profile");
 countsBragg->SetMarkerColor(kOrange+8);
 countsBragg->SetLineColor(kOrange+8);
 countsBragg->SetMarkerStyle(34);
 countsBragg->SetMarkerSize(1.2);
 countsBragg->SetLineWidth(3);
 countsBragg->GetXaxis()->SetTitle("depth (mm)");
 countsBragg->GetYaxis()->SetTitle("Number of Tracks");
 countsBragg->GetYaxis()->SetTitleOffset(1.1);

 TNamed *trimInfo = new TNamed("***** TRIM ******","");  
 TNamed *srimRange050 = new TNamed("range50_TRIM", Form("%3.2f mm", srim050));
 TNamed *EnergySRIM = new TNamed("Energy_TRIM", Form("%6.2f keV", Isrim));
 
 TNamed *liseInfo = new TNamed("***** LISE++ ******",""); 
 TNamed *liseRange050 = new TNamed("range50_LISE++", Form("%3.2f mm", lise050));
 TNamed *EnergyLISE = new TNamed("Energy_LISE++", Form("%6.2f keV", Ilise)); 
 
 TNamed *myResultsList = new TNamed("***** RESULTS ******","");  
 
 TNamed *cutsInfo1 = new TNamed("chargeCut", Form("MEAN: %3.3f NSIGMA: %1.0f SIGMA: %3.3f", meanCharge, nsigmaCharge, sigmaCharge));
 TNamed *cutsInfo2 = new TNamed("range50Cut", Form("MEAN: %3.3f NSIGMA: %1.0f SIGMA: %3.3f", meanRange, nsigmaRange, sigmaRange));
 TNamed *cutsInfo3 = new TNamed("npadsCut", Form("MEAN: %3.3f NSIGMA: %1.0f SIGMA: %3.3f", meanNpads, nsigmaNpads, sigmaNpads));
 
 TNamed *TAIL = new TNamed("TAIL",Form("x>=%3.2f mm", tail));
 TNamed *FRONT = new TNamed("FRONT",Form("x<=%3.2f mm", front));
 
 TNamed *sigmaDiff = new TNamed("sigmaDiff",Form("Sigma Diffusion: %3.2f mm", sigma));
 
 TNamed *numOfTracks = new TNamed(Form("SELECTED TRACKS: %d (%2.3f)", accumulator, 100.*accumulator/(lastEntry-firstEntry)),"");
 
 Results->cd();
 
 trimInfo->Write();
 srim->Write();
 EnergySRIM->Write();
 srimRange050->Write();

 liseInfo->Write();
 lise->Write();
 EnergyLISE->Write();
 liseRange050->Write();
  
 myResultsList->Write();

 TAIL->Write();
 FRONT->Write(); 
 
 sigmaDiff->Write();
 
 cutsInfo1->Write();
 cutsInfo2->Write();
 cutsInfo3->Write();

 numOfTracks->Write();
 avgTrack->Write();
 
 maxPos->Write();
 diffusion->Write();
 countsBragg->Write();
 erelProfile->Write();
 braggProfile->Write();  


 
 #ifdef _WRITE_
 
 for(int hh=0; hh<NCOLS; hh++)
 {
  oneColumnProfile[hh]->Write();
  //oneColumnDiffusion[hh]->Write();
  //oneColumnPosition[hh]->Write();
 }
 #else
 
 for(int hh=0; hh<NCOLS; hh++)
 {
  delete oneColumnProfile[hh];
  delete oneColumnDiffusion[hh];
  delete oneColumnPosition[hh];
 } 
 #endif
 
 data->Write();
 _file_->Close();
 Results->Close();
}


/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/
/*********************************************************************/

			/*
				selection order:
				charge    range050    Npads    theta    phi
			*/
TGraphErrors *SelectionBragg(TTree *tree, array<bool, numOfSelVars> selections, array<double, numOfSelVars> nSigmas, TGraph *srim = NULL, TCutG *selCut = NULL)
{ 
 TGraphErrors *tempBragg = new TGraphErrors(NCOLS);
  
 array<string, numOfSelVars> selectionVariables = {"charge", "range050", "Npads", "theta", "phi"};
 array<string, numOfSelVars> intervalVariables = {"(3000,0,300000)","(200,0,200)","(800,0,800)","(1000,-50,50)","(1000,-50,50)"}; 
 
 tree->Draw("range005>>htemp(200,0,200)","","goff");
 TH1F *hRange005 = (TH1F*)gROOT->FindObject("htemp");
 TF1 rangeFit("rangeFit","gaus(0)", 0,200);
 hRange005->Fit("rangeFit","RNQ");
 double endProfile = rangeFit.GetParameter(1)+padSize;
   
 int words;
 double n, a, b;
 
 array<double, numOfSelVars> centroids;
 array<double, numOfSelVars> sigmas;
 
 for(int kk=0; kk<numOfSelVars; kk++)
 {
  if(!selections[kk])
  {
   centroids[kk]=-1;
   sigmas[kk]=-1;
   continue;  
  }
  tree->Draw(Form("%s>>h%s%s",selectionVariables[kk].c_str(), selectionVariables[kk].c_str(), intervalVariables[kk].c_str()),"","goff");
  TH1F *h = (TH1F*)gROOT->FindObject(Form("h%s", selectionVariables[kk].c_str()));
  
  words = sscanf(intervalVariables[kk].c_str(), "(%lf,%lf,%lf)", &n, &a, &b);
  
  TF1 *gausfun = new TF1("gausfun","gaus(0)",a,b);
  gausfun->SetParameter(0, h->GetMaximum()); 
  gausfun->SetParameter(1, h->GetMean());
  h->Fit("gausfun","RNQ");

  double mean = gausfun->GetParameter(1); 	
  double sigma = gausfun->GetParameter(2);
  
  
    cout<<endl<<selectionVariables[kk].c_str()<<endl;
    cout<<"mean: "<<mean<<endl;
    cout<<"sigma: "<<sigma<<endl;
    cout<<"nSigma: "<<nSigmas[kk]<<endl;
  
  
  centroids[kk] = gausfun->GetParameter(1);
  sigmas[kk] = gausfun->GetParameter(2);
     
  delete h;
  delete gausfun;
 }
 
 double charge, range050, Npads, theta, phi; 
 bool condCharge, condRange, condNpads, condTheta, condPhi;
 
 bool cond1, cond2;
 
 const double dQdxMax = 5000.;
 TH1D *oneColumnProfile[NCOLS];
 TF1 *gausProfile = new TF1("gausProfile","gaus(0)",0.,dQdxMax);
 gausProfile->SetParLimits(1, 0, dQdxMax);
 gausProfile->SetParLimits(2, 0, dQdxMax);
 
 for(int hh=0; hh<NCOLS; hh++)
 	oneColumnProfile[hh] = new TH1D(Form("oneColumnProfile_%d",hh),Form("oneColumnProfile_%d",hh), dQdxMax/5, 0., dQdxMax);

 TTreeReader elossReader(tree); 
 TTreeReaderValue<int> eventValue(elossReader, "event"); 

 TTreeReaderValue<double> chargeValue(elossReader, "charge");
 TTreeReaderValue<double> range050Value(elossReader, "range050");
 
 TTreeReaderValue<int> NpadsValue(elossReader, "Npads");
 
 TTreeReaderValue<double> thetaValue(elossReader, "theta");
 TTreeReaderValue<double> phiValue(elossReader, "phi");
  
 TTreeReaderValue<TGraph> evProfileValue(elossReader, "evProfile");
 
 int count=0;
 
 while(elossReader.Next())
 {
  condCharge = true;
  condRange = true;
  condNpads = true;
  condTheta = true;
  condPhi = true;
  
  charge = chargeValue.Get()[0];  
  if(centroids[0]>0 && sigmas[0]>0)
  	condCharge = Abs(charge-centroids[0])<nSigmas[0]*sigmas[0];
  
  range050 = range050Value.Get()[0];  
  if(centroids[1]>0 && sigmas[1]>0)
  	condRange = Abs(range050-centroids[1])<nSigmas[1]*sigmas[1];
  	
  Npads = NpadsValue.Get()[0];  
  if(centroids[2]>0 && sigmas[2]>0)
  	condNpads = Abs(Npads-centroids[2])<nSigmas[2]*sigmas[2];
  	
  theta = thetaValue.Get()[0];  
  if(centroids[3]>0 && sigmas[3]>0)
  	condTheta = Abs(theta-centroids[3])<nSigmas[3]*sigmas[3];

  phi = phiValue.Get()[0];  
  if(centroids[4]>0 && sigmas[4]>0)
  	condPhi = Abs(phi-centroids[4])<nSigmas[4]*sigmas[4];
  	
  cond1 = condCharge && condRange && condNpads && condTheta && condPhi;
   
  if(!cond1)
  	continue;

  if(selCut==NULL)
  { 
   cond2 = true;
   count++;
  }
  else
  {
   TString s(selCut->GetName());  
   s.ToLower();
   
   if(s.Contains("charge") && s.Contains("range"))
   {
    cond2 = selCut->IsInside(range050, charge);
    if(cond2)
    	count++;
   }
   else 
   {
    if(s.Contains("charge") && s.Contains("npads"))
    {
     cond2 = selCut->IsInside(Npads, charge);
     if(cond2)
    	count++;
    }
    else 
    {
     if(s.Contains("range") && s.Contains("npads"))
     {
      cond2 = selCut->IsInside(Npads, range050);
      if(cond2)
    	count++;
     }
     else
     {
      cond2 = true;
      if(cond2)
    	count++;
     }
    }
   }
  }
    
  if(!cond2)
  	continue;
  	  	
  auto profile = evProfileValue.Get()[0];  
  double *xprofile = profile.GetX(); 
  double *yprofile = profile.GetY(); 
  
  for(int ii=0; ii<NCOLS; ii++)
  {
   if(yprofile[ii]>0.)
	   oneColumnProfile[ii]->Fill(yprofile[ii]);
  }
 }//end of while
 
  for(int ii=0; ii<NCOLS; ii++)
  {  
   double distanceBragg = padSize*ii;
   if(distanceBragg<endProfile)
   {
    oneColumnProfile[ii]->Fit("gausProfile","RNQ");
   
    double _mu_ = gausProfile->GetParameter(1);  
    double _sigma_ = gausProfile->GetParameter(2);
  
   tempBragg->SetPoint(ii,distanceBragg , _mu_);
   tempBragg->SetPointError(ii, padSize/2.,_sigma_);
   }
   else
   {
   tempBragg->SetPoint(ii,distanceBragg, 0);
   tempBragg->SetPointError(ii, padSize/2., 0);
   }
   
   delete oneColumnProfile[ii];
  }

 if(srim==NULL)
 {
  tempBragg->SetMarkerStyle(20);  
  return tempBragg;
 }
 else
 {
  double cal = ComputeCalibration(tempBragg, srim);
  
  TGraphErrors *bragg = ScaleYErrors(tempBragg, cal);
  bragg->SetMarkerStyle(20);
  bragg->SetMarkerColor(kRed+1);
  bragg->SetLineColor(kRed+1);
  
  
  cout<<count<<endl;
  
  return bragg;
 }
}

