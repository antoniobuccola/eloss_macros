
#ifndef LIBELOSS_H
#define LIBELOSS_H

#define AMU 931500	//	keV/c**2
#define Clight 300	//	mm/ns

#define nInt 5000
/*
	used for Cavalieri - Simpson's rule
	to compute the integral of a TGraph
	nInt MUST BE EVEN
	
	     (b-a)^5     
	e ~ __________
	 
	     nInt^4       
*/

#define DBGLV 0
#if DBGLV>0
	#define DEBUGENTRIES 10
#endif

#define NCOLS 64
const double aSide = NCOLS*2.;

#define NROWS 32
const double bSide = NCOLS*2.;

//#define _WRITE_ 1

#define HEIGHT 100 /* mm */

#define padSize  2. // mm 
#define padPitch 0.08 // mm (0.08)

#define _START_ 50. 
// 50. for active gas
// 0 for mylar or other materials (...)

#define _LOW_ 2.

#define _COUT_STEP_ 10000 /* step for "cout" */

#define _NPADS_CUT_ 20 /* reference value: 20 (minimum) */

#define _BRAGG_ENTRIES_CUT_ 10  /* reference value: 5 (minimum) */

#define _THETA_CUT_ 5 /* reference value(s): 2 - 5 (maximum) N.B. >90 -> no cut */

#define _PHI_CUT_ 5 /* reference value(s): 2 - 5 (maximum) N.B. >90 -> no cut */

#define integral_do 3
#define integral_up 28 
/*
	--- !! Check the actual size of the track !! ---
*/
#define _FRACTION_ 0.005

/* 
	for EVENT SELECTION
*/
#define nsigmaCharge 2.
#define nsigmaRange 2.
#define nsigmaNpads 2.
#define nsigmaTheta 20.
#define nsigmaPhi 20.

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TProfile.h"
#include "TF1.h"
#include "TSpline.h"
#include "TFile.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TBranch.h"
#include "TCut.h"
#include "TCutG.h"
#include "TMatrixD.h"
#include "TString.h"
#include "TMath.h"
#include "Math/ProbFuncMathCore.h"

using namespace TMath;

#include "TVector3.h"
#include "TNamed.h"
#include "TMarker.h"

#include "pSignal.h"
#include "pCFD.h"
#include "pDSP.h"

#include "cLookupTable.h"
#include "cRawEvent.h"
#include "cHit.h"
#include "cRawSignal.h"

TVector3 O(0., 16., 0.);

TH2F* BuildTrack(TTree *rawEventTree, cLookupTable *lookupTable, Int_t eventNumber, double th);

TH2F* BuildTrackWithGains(TTree *rawEventTree, cLookupTable *lookupTable, TMatrixD gains, Int_t eventNumber, double th);

TH2F *BuildSideView(TTree *rawEventTree, cLookupTable *lookupTable, double vdrift, double delay, double Ts, Int_t eventNumber, double th);

pSignal *Histo2Sig(TH1 *histo);

TGraph *ATS2Physics(TH1 *histo, double scaling);

TGraph *srimBragg(const char *filename);

TGraph *Diffuse(const char *filename, double sigma);

TMatrixD GetGains(TString Ltablefile, double G0);

TH2F *MapOfGains(TString Ltablefile, double G0);

double GetStart(TH1 *histo);

double TransverseWidth(TH2 *track);

TGraph *DiffusionWidth(TH2* track);

double GetTheta(TH2 *track, int a, int b);

double GetPhi(TH2 *side, int a, int b);

double GetYaverage(TH2 *track);

double GetRangeGraph(TGraph *graph, double f);

double GetExtrapolatedRangeGraph(TGraph *graph);

array<double, 3> GetExtrapolatedRangeGraphErrors(TGraphErrors *graph);

TGraph *RangeVsCharge(TSpline3 braggSpline, double start, double step);

TGraph *RangeVsCharge(TGraph *braggGraph, double scale, double step);

TGraph *RangeVsCharge(TH1 *braggHisto, double scale, double step);

TGraph *ScaleY(TGraph *gr, double scale);

TGraphErrors *ComputeAbsoluteResiduals(TGraphErrors *g1, TGraph *g2); /* g1 minus g2 */

TGraphErrors *ComputeRelativeResiduals(TGraphErrors *g1, TGraph *g2); /* (g1 minus g2)/ g2 */

double IntegrateProfile(TSpline3 profile, double a, double b);

double IntegrateProfile(TGraph *profile, double a, double b);

array<double, 2> IntegrateProfileWithErrors(TGraphErrors *profile, double a, double b);

double GetMaxGraph(TGraph *gr);

double ComputeCalibration(TGraph *ats, TGraph *srim); /* ATS is supposed to be without ENERGY calibration */

double GetChi2(TGraphErrors *calATS, TGraph *srim);

double GetChi2(TGraphErrors *ats, TGraph *srim, double cal);

TGraphErrors *RangeVsChargeWithErrors(TGraphErrors *braggGraph, double step);

array<double, 2> ComputeCalibrationAndError(TGraphErrors *ats, TGraph *srim);

TGraphErrors *ScaleYErrors(TGraphErrors *gr, double scale, double escale);

TGraphErrors *ShiftXwithErrors(TGraphErrors *gr, double shift);

TGraph *ShiftX(TGraph *gr, double shift);

/**************************************/

array<TGraphErrors*, 2> elossVsSpeed(TGraphErrors *ats, TGraph *srim, int A);

TGraphErrors *elossVsEnergyATS(TGraphErrors *profile); 

TGraph *elossVsEnergySRIM(TGraph *profile);

/*

TGraphErrors *elossVsSpeedATS(TGraphErrors *profile, int A);

TGraph *elossVsSpeedSRIM(TGraph *profile, int A);

*/

TGraphErrors *stoppingXsection_vs_energy(TGraphErrors *profile, double density, double gas_molar_mass); 

/******* ANALYZERS *******/

			/* 
				analysis with all variables in the output TTree
				
				DEPRECATED
			*/
void makeElossTree(TString filename, TString oname, TString table, Int_t Entries, double th);

			/* 
				analysis with variables in the output TTree
				from pulser equalization only
			*/
void makeElossTreeEqualizationOnly(TString filename, TString oname, TString table, double vdrift, double delay,  double Ts, Int_t Entries, double th);

			/* 
				analysis  with variables in the output TTree
				from pulser equalization only
				and a fast comparison with calculation 
				(SRIM or some other model)
			*/
void makeElossTreeFastComparison(TString filename, TString oname, TString table, TString model, double calib, Int_t Entries, double th); 

			/* 								
				analysis with variables in the output 
				TTree from pulser equalization only
				the TTree contains the raw track and the Bragg profile event-by-event
				cuts will NOT be applied, except minimal ones on histogram entries:
				* entries on the TH2 representing the track > _NPADS_CUT_  (reference value: 20)
				* entries on the TH1 representing the Bragg profile > _BRAGG_ENTRIES_CUT_  (reference value: 10)
				
					HIGHLY RECOMMENDED
			*/
void makeElossTreeWithProfiles(TString filename, TString oname, TString table, double vdrift, double delay,  double Ts, Int_t Entries, double th); 

/******* RESULTS *******/

void StoreResults(TString elossFile, TString results, TString model, double sigma, double tail, double front, int firstEntry, int lastEntry, double cutRange, double step);

const int numOfSelVars=5;
TGraphErrors *SelectionBragg(TTree *tree, array<bool, numOfSelVars> selections, array<double, numOfSelVars> nSigmas, TGraph *srim, TCutG *selCut);

#endif
