#!/bin/bash
#

cp ../libEloss.* .
cp ../calcLISE/LISEreader.C .
cp ../_scripts_/convertToDataTrack.cc .
cp ../_scripts_/convertToProfileInfo.C .
cp ../_scripts_/convertToIdentification.C .
cp ../_scripts_/checkOnSignals.C .
cp ../_scripts_/plotProducer.C .

echo Analysis energy loss - Insert comment to \"commit\"

read commit

git add .
git commit -m "$commit"
git push -u origin master

